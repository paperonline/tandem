<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', function () {
    return view('home');
})->middleware('auth');


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/profile', 'ProfileController@index')->name('profile');

Route::resource('/compagny', 'CompagnyController')->middleware('auth');
Route::post('/compagny/store', 'CompagnyController@store')->name('posts');
Route::put('/compagny/update','CompagnyController@update')->name('update');
Route::post('/compagny/changeStatus', 'CompagnyController@changeStatus')->name('changeStatus');


Route::resource('/userController','UserContoller')->middleware('auth');
//Route::post('/userController/store','UserContoller@store')->name('posts');
//Route::put('/userController/update','UserContoller@update')->name('update');
Route::post('/userController/changeStatus', 'UserContoller@changeStatus')->name('changeStatus');
// Route::post('/userController/changeStatus', array('as'=>'changeStatus','uses'=>'UserContoller@changeStatus'));

Route::resource('/timesheet','TimeSheetController')->middleware('auth');
Route::post('/timesheet/store','TimeSheetController@store')->name('timesheet.store');
Route::put('/timesheet','TimeSheetController@sheetupdate')->name('sheetupdate');
Route::post('/timesheets/selectTache','TimeSheetController@selectTache')->name('selectTache');
Route::get('/timesheets/search','TimeSheetController@search')->name('search');
//Route::get('/timesheet/{getuuid}', 'TimeSheetController@getuuid')->name('getuuid');
//Route::resource('/timesheetlist/getuuid','ListTimeSheetController');
Route::post('/timesheet','TimeSheetController@getuuid')->name('getuuid');
Route::post('/timesheet/exportAdmin','TimeSheetController@exportAdmin')->name('exportAdmin');
Route::post('/timesheet/active','TimeSheetController@active')->name('active');
Route::post('/timesheet/addSheet','TimeSheetController@addSheet')->name('addSheet');
Route::post('/timesheet/deleteSheet','TimeSheetController@deleteSheet')->name('deleteSheet');

Route::resource('/departement', 'DepartementController')->middleware('auth');

Route::resource('/tache', 'TacheController')->middleware('auth');

Route::get('/timesheet-csv', function() {

    $headers = array(
          'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
          'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
          'Content-Disposition' => 'attachment; filename=abc.csv',
          'Expires' => '0',
          'Pragma' => 'public',
      );

  $filename = "timesheets.csv";
  $handle = fopen($filename, 'w');
  fputcsv($handle, [
      'id',
      'user_id',
      'employe',
      'date',
      'dossier',
      'client',
      'titreTache',
      'debut',
      'fin',
      'temps',
      'departement',
      'codeTache',
      'comment',
      'uuidTimeSheet',
      '_token',
      'nbligne',
      'created_at',
      'updated_at'
  ]);

  DB::table("timesheets")->orderBy('employe')->chunk(100, function ($data) use ($handle) {
      foreach ($data as $row) {
          // Add a new row with data
          fputcsv($handle, [
            $row->id,
            $row->user_id,
            $row->employe,
            $row->date,
            $row->dossier,
            $row->client,
            $row->titreTache,
            $row->debut,
            $row->fin,
            $row->temps,
            $row->departement,
            $row->codeTache,
            $row->comment,
            $row->uuidTimeSheet,
            $row->_token,
            $row->nbligne,
            $row->created_at,
            $row->updated_at
          ]);
      }
  });

  fclose($handle);

  return Response::download($filename, "timesheets.csv", $headers);

});
