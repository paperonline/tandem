<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employe');
            $table->string('date');
            $table->string('dossier');
            $table->string('client');
            $table->string('titreTache');
            $table->float('debut', 2, 2);
            $table->float('fin', 2, 2);
            $table->float('temps', 2, 1);
            $table->string('departement');
            $table->string('codeTache');
            $table->string('comment');
            $table->string('uuidTimeSheet');
            $table->string('_token');
            $table->string('nbligne');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets');
    }
}
