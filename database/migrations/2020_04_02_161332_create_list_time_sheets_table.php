<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListTimeSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_time_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('timesheet_id');
            $table->string('nomEmploye');
            $table->string('jours');
            $table->string('date');
            $table->string('absence');
            $table->string('active');
            $table->string('exporte');
            $table->string('date_exporte');
            $table->string('temps_excute');
            $table->uuid('uuidTimeSheet');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_time_sheets');
    }
}
