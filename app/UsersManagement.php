<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersManagement extends Model
{
    protected $table='users_managements';
    protected $fillable = ['_token','active','name','email','password','confirmpassword','role'];

    public function timesheets()
    {
        return $this->hasMany(Timesheet::class);
    }

}
