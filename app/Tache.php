<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tache extends Model
{
    protected $table='taches';

    public function departement()
    {
      return $this->hasMany('App\Departement');
    }
}
