<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    protected $table='departements';

    public function tache()
    {
      return $this->belongsTo('App\Tache');
    }
}
