<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compagny extends Model
{
  protected $table = 'compagnies';
  protected $fillable = ['_token','namecompagny','firstname','lastname','email','streetaddress','city','stateregion','country','codepostal',
  'phonenumber','active'];
}
