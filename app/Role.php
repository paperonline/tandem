<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    // protected $table='role_user';

    public function users()
    {
      return $this->belongsToMany(UsersManagement::class);
    }
}
