<?php

namespace App\Http\Controllers;

use App\Tache;
use App\Departement;
use Validator;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class TacheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $rules =
     [   '_token'          => 'required',
         'code'            => 'required|min:4',
         'name'            => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
     ];

    public function index()
    {
      $taches = Tache::paginate(5);
      $departements = Departement::all();
      $tacheAjax = Tache::all();

      return view('tache',['taches'=>$taches,'departements'=>$departements, 'tacheAjax'=>$tacheAjax]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request != NULL){
        }
          $validator = Validator::make(Input::all(), $this->rules);
          if ($validator->fails()) {
              return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
          } else {

          $departement_id = Departement::where('id',$request->departements)->first();

              $tache = new Tache;
              $tache->code_tache      = $request->code;
              $tache->nom_tache      = $request->name;
              $tache->departement_id  = $departement_id->id;

              $tache->save();

              $return = new Tache;

              $departement_name = Departement::where('id', $departement_id->id)->first();

              $return->id               = $tache->id;
              $return->code             = $tache->code_tache;
              $return->name             = $tache->nom_tache;
              $return->departement_name = $departement_name->nom_departement;

              return response()->json($return);
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tache  $tache
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tache  $tache
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tache  $tache
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tache $tache)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tache  $tache
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $tache = Tache::findOrFail($id);
      $tache->delete();

      return response()->json($tache);
    }
}
