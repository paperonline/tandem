<?php

namespace App\Http\Controllers;

use App\timesheet;
use App\User;
use App\ListTimeSheet;
use App\Departement;
use App\Tache;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Session;
use Illuminate\Support\Facades\Auth;
use view;
use Response;
use Validator;
use Webpatser\Uuid\Uuid;

class TimeSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      Date::setLocale('fr');

      $jours = Date::now()->format('l j F Y');
      $name = Auth::user()->name;

      $timesheet = timesheet::all();

      if(Auth::user()->role == 'administrateur'){
        $listTimeSheet = ListTimeSheet::paginate(5);
      }elseif (Auth::user()->name == $name) {
        $listTimeSheet = ListTimeSheet::where('nomEmploye', '=', $name)->paginate(10);
      }

      $userManagement = User::all();
      $departement = Departement::all();
      $timesheetEdit = timesheet::all();
      $taches = Tache::all();

      return  view('timesheet',['jours'=>$jours,'timesheet'=>$timesheet,'userManagement'=>$userManagement, 'listTimeSheet'=>$listTimeSheet, 'timesheetEdit'=>$timesheetEdit, 'departement'=>$departement, 'taches'=>$taches]);
    }

    public function uuidtimesheet()
    {
        return $this->hasMany('App\timesheet', 'uuidTimeSheet', 'uuidTimeSheet');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Request $request)
     {
       $joursFr = [
         '1' => 'Lundi',
         '2' => 'Mardi',
         '3' => 'Mercredi',
         '4' => 'Jeudi',
         '5' => 'Vendredi',
         '6' => 'Samedi',
         '7' => 'dimanche',
       ];

        if($request->ajax())
        {
         $rules = array(
          'dossier.*'  => 'required',
          'client.*'  => 'required',
          'titreTache.*'  => 'required',
          'debutHr.*'  => 'required',
          'finHr.*'  => 'required',
          'departement.*'  => 'required|not_in:Choisir...',
          'tache.*'  => 'required|not_in:Choisir...',
         );
         $error = Validator::make($request->all(),$rules);
         if($error->fails())
         {
          return response()->json([
           'error'  => $error->errors()->all()
          ]);
         }

         $userIdLog = Auth::id();
         $userLog = Auth::user()->name;
         $uuidTimeSheet = Uuid::generate(4);

         $timeSheet = new timesheet;

         // $timeSheet->user_id      = $userIdLog;
         // $timeSheet->employe      = $userLog;
         $_token       = $request->_token;
         $date         = $request->date_add;
         $dossier      = $request->dossier;
         $client       = $request->client;
         $titreTache   = $request->titreTache;
         $debut        = $request->debutHr;
         $fin          = $request->finHr;
         $departement  = $request->departement;
         $codeTache    = $request->tache;
         $comment      = $request->comment;

          $tabDate = explode('/', $date);
          $timestamp = mktime(0, 0, 0, $tabDate[1], $tabDate[0], $tabDate[2]);
          $jour = date('w', $timestamp);

         for($count = 0; $count < count($dossier); $count++)
         {
           $h1[$count] = str_replace(':', '.', $fin[$count]);
           $h2[$count] = str_replace(':', '.', $debut[$count]);
           $time[$count] = $h1[$count] - $h2[$count];

          $data = array(
           'user_id' => $userIdLog,
           'employe' => $userLog,
           'date'    => $date,
           'uuidTimeSheet'    => $uuidTimeSheet,
           '_token' => $_token[$count],
           'dossier' => $dossier[$count],
           'client'  => $client[$count],
           'titreTache'  => $titreTache[$count],
           'debut'  => $debut[$count],
           'fin'  => $fin[$count],
           'temps'  => $time[$count],
           'departement'  => $departement[$count],
           'codeTache'  => $codeTache[$count],
           'comment'  => $comment[$count],
           'nbligne'  => count($dossier)
          );
          $insert_data[] = $data;
         }
         $timeTotal = array_sum($time);

         timesheet::insert($insert_data);

         $listTimeSheet = new ListTimeSheet;

         $listTimeSheet->nomEmploye      = $userLog;
         $listTimeSheet->uuidTimeSheet   = $uuidTimeSheet;
         $listTimeSheet->date            = $date;
         $listTimeSheet->jours           = $joursFr[$jour];
         $listTimeSheet->temps_excute    = $timeTotal;

         $listTimeSheet->save();

         return response()->json([
          'success'  => 'Data Added successfully.'
         ]);
        }
   }


    /**
     * Display the specified resource.
     *
     * @param  \App\timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function addSheet(Request $request)
    {
          $userIdLog = Auth::id();
          $userLog = Auth::user()->name;
          $uuidTimeSheet = $request->uuid;
          $_token = $request->_token;
          $date = \Carbon\Carbon::now()->format('d/m/Y');
          //$tabDate = explode('/', $date);
          $data = array(
            'user_id' => $userIdLog,
            'employe' => $userLog,
            'date'    => $date,
            'uuidTimeSheet'    => $uuidTimeSheet,
            '_token' => $_token,
            'created_at'  => \Carbon\Carbon::now()->toDateTimeString()
           );
           $insert_data[] = $data;
           timesheet::insert($insert_data);
           $lastId = DB::getPdo()->lastInsertId();

           $response=array('id'=>$lastId);
           return response()->json($response,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function sheetupdate(Request $request)
    {
        $id = $request->id;
        $employe = $request->employe;
        $dossier = $request->dossier;
        $client = $request->client;
        $titreTache = $request->titreTache;
        $debut = $request->debut;
        $fin = $request->fin;
        $departement = $request->departement;
        $codeTache = $request->codeTache;
        $comment = $request->comment;

        foreach ($id as $key => $value) {
          $h1[$key] = str_replace(':', '.', $fin[$key]);
          $h2[$key] = str_replace(':', '.', $debut[$key]);
          $time[$key] = $h1[$key] - $h2[$key];

         DB::table('timesheets')->where('id', $id[$key] )->update([

              'employe'     => $employe[$key],
              'dossier'     => $dossier[$key],
              'client'      => $client[$key],
              'titreTache'  => $titreTache[$key],
              'debut'       => $debut[$key],
              'fin'         => $fin[$key],
              'temps'       => $time[$key],
              'departement' => $departement[$key],
              'codeTache'   => $codeTache[$key],
              'comment'     => $comment[$key],
              'updated_at'  => \Carbon\Carbon::now()->toDateTimeString()
             ]);
        }
        // $timeTotal = array_sum($time);
        // $listTimeSheet = new ListTimeSheet;
        // $listTimeSheet->temps_excute    = $timeTotal;
        //
        // $listTimeSheet->update();

        return response()->json([
         'success'  => 'Data Added successfully.'
          ]);
    }

    public function deleteSheet(Request $request){
      $id = $request->id;
      $timeSheet = timesheet::findOrFail($id);
      $timeSheet->delete();

      return response()->json($timeSheet);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $data = $request->uuid;
      //$sheets = timesheet::where('uuidTimeSheet',$data)->get();
      timesheet::where('uuidTimeSheet',$data)->delete();

      $listTimeSheet = ListTimeSheet::findOrFail($id);
      $listTimeSheet->delete();

      return response()->json($listTimeSheet);
    }

    public function active(Request $request)
    {
      $id = $request->id;
      $check = $request->check;

      DB::table('list_time_sheets')->where('id', $id )->update([
                 'active'    => $check,
        ]);

      return response()->json([
          'success'  => 'Data Added successfully.'
      ]);
    }

    public function exportAdmin(Request $request)
    {
        $id = $request->id;
        $check = $request->check;
        if($check == 1)
        {
          $todayDate = $request->todayDate;
        }
        else
        {
          $todayDate = '-';
        }


        DB::table('list_time_sheets')->where('id', $id )->update([
           'exporte'    => $check,
           'date_exporte'=>$todayDate
        ]);

        return response()->json([
         'success'  => 'Data Added successfully.'
        ]);
    }

    public function getuuid(Request $request) {

        $datatest = $request->uuid;
        $employe  = $request->employe;

          $sheets = timesheet::where('uuidTimeSheet',$datatest)->get();


          foreach ($sheets as $sheet) {
            $data = array(
                'id' => $sheet->id,
                'date'=> $sheet->date,
                'dossier' => $sheet->dossier,
                'client' => $sheet->client,
                'titreTache' => $sheet->titreTache,
                'debut' => $sheet->debut,
                'fin' => $sheet->fin,
                'temps' => $sheet->temps,
                'departement' => $sheet->departement,
                'codeTache' => $sheet->codeTache,
                'comment' => $sheet->comment,
                'uuid' => $sheet->uuidTimeSheet,
                'employe' => $employe
            );
            $output[] = $data;
          }

          $response=array('output'=>$output);
          return response()->json($response,200);

    }

    public function selectTache(Request $request){
      $selectDepart = $request->selectDepart;
      $selectId = DB::table('departements')->where('nom_departement', $selectDepart )->first();
      $id = $selectId->id;
      $selectTache = Tache::where('departement_id','=',$id)->get();
      $selectTache=array('selectTache'=>$selectTache);
      return response()->json($selectTache,200);
    }

    public function search(Request $request)
    {
      if($request->ajax())
      {
        $timeSheets=DB::table('list_time_sheets')->where('nomEmploye','LIKE','%'.$request->search."%")->get();

        foreach ($timeSheets as $timeSheet) {
          $data = array(
              'id' => $timeSheet->id,
              'nomEmploye' => $timeSheet->nomEmploye,
              'jours' => $timeSheet->jours,
              'date'=> $timeSheet->date,
              'absence' => $timeSheet->absence,
              'active' => $timeSheet->active,
              'exporte' => $timeSheet->exporte,
              'date_exporte' => $timeSheet->date_exporte,
              'temps_excute' => $timeSheet->temps_excute
          );
          $output[] = $data;
        }

        $response=array('output'=>$output);
        return response()->json($response,200);

       }
    }

}
