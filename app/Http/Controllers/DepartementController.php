<?php

namespace App\Http\Controllers;

use App\Departement;
use Validator;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DepartementController extends Controller
{

      protected $rules =
      [   '_token'          => 'required',
          'code'            => 'required|min:4',
          'name'            => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
      ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $departements = Departement::paginate(10);

      return view('departement',['departements'=>$departements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request != NULL){
      }
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

            $departement = new Departement;
            $departement->code_departement     = $request->code;
            $departement->nom_departement      = $request->name;

            $departement->save();

            $return = new Departement;

            $return->id               = $departement->id;
            $return->code             = $departement->code_departement;
            $return->name             = $departement->nom_departement;

            return response()->json($return);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Departement  $departement
     * @return \Illuminate\Http\Response
     */
    public function show(Departement $departement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departement  $departement
     * @return \Illuminate\Http\Response
     */
    public function edit(Departement $departement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Departement  $departement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departement $departement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Departement  $departement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $departement = Departement::findOrFail($id);
          $departement->delete();

          return response()->json($departement);
    }
}
