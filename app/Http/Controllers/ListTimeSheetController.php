<?php

namespace App\Http\Controllers;

use App\ListTimeSheet;
use Illuminate\Http\Request;
use App\timesheet;
use App\UsersManagement;
use view;
use Response;
use Validator;
use Webpatser\Uuid\Uuid;

class ListTimeSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $timesheet = timesheet::all();
      $listTimeSheets = ListTimeSheet::all();

      return  view('timesheetlist',[ 'listTimeSheets'=>$listTimeSheets, 'timesheet'=>$timesheet]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //$listTimeSheet = new ListTimeSheet;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ListTimeSheet  $listTimeSheet
     * @return \Illuminate\Http\Response
     */
    public function show(ListTimeSheet $listTimeSheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ListTimeSheet  $listTimeSheet
     * @return \Illuminate\Http\Response
     */
    public function edit(ListTimeSheet $listTimeSheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ListTimeSheet  $listTimeSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListTimeSheet $listTimeSheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ListTimeSheet  $listTimeSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListTimeSheet $listTimeSheet)
    {
        //
    }
}
