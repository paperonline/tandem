<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


      /** Get the needed authorization credentials from the request.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param use App\User $user
      * @return array
      */


      protected function authenticated(Request $request, $user)
      {
         if(!$user->active) {
             Auth::logout();
             Session::flush();
             return redirect(url('login'))->withInput()->with('errorMsg','You are temporary blocked. please contact to admin');
         };
      }


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(){
      Auth::logout();
      Session::flush();
      return redirect('/');
    }
}
