<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use App\User;
use App\Role;
use App\Compagny;
use Webpatser\Uuid\Uuid;
use view;
use Illuminate\Support\MessageBag;
//use Alphametric\Validation\Rules\StrongPassword;

class UserContoller extends Controller
{
    protected $rules =
    [   '_token'          => 'required',
        'active'          => 'required',
        'name'            => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
        'email'           => 'bail|required|email',
        'password'        => 'required|min:12|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{12,}$/|required_with:confirmpassword|same:confirmpassword',
        'confirmpassword' => 'required|min:12',
        'role'            => 'required',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index()
    {
        $users = User::paginate(5);
        $compagny = Compagny::all();
        $roles = Role::whereNotIn('id', array(1))->get();

        return view('userController',['users'=>$users, 'compagny'=>$compagny, 'roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      $messages = [
          'password.regex' => 'Password must contain at least 12 characters, including UPPER/lowercase, numbers and special character',
      ];

      if ($request != NULL){
      }
        $validator = Validator::make(Input::all(), $this->rules, $messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

        $role = Role::where('name',$request->role)->first();

            $user = new User;
            $user->remember_token   = $request->_token;
            $user->active           = $request->active;
            //$userManagement->uuidCompagnyPo   = $request->compagny;
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->password         = Hash::make($request['password']);
            $user->role             = $role->name;
            $user->role_id          = $role->id;

            //$userManagement->uuidUserPo = Uuid::generate(4);
            $user->save();
            $token = $request->_token;
            $id = User::select('id')->where('remember_token', $token)->first()->id;
            $user_id = $role->id;
            $role_id = $id;

            \DB::table('role_user')->insert(
              ['user_id' => $user_id,
              'role_id' => $role_id,
              'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
              'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);

            $return = new User;

            $return->id               = $user->id;
            $return->name             = $user->name;
            $return->email            = $user->email;
            $return->role             = $request->role;

            return response()->json($return);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('userController.show',['User'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $userManagement = UserManagement::findOrFail($id);
    //     $userManagement->save();
    //     return response()->json($userManagement);
    //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { if ($request->password == "")
      {
          $request->role;

          $role = Role::where('id',$request->role)->first();

          $user = User::findOrFail($id);
          $user->remember_token   = $request->_token;
          $user->active           = $request->active;
          $user->name             = $request->name;
          $user->email            = $request->email;
          $user->role             = $role->name;
          $user->role_id          = $role->id;


          $user->save();

          $return = new User;

          $return->id               = $user->id;
          $return->name             = $user->name;
          $return->email            = $user->email;
          $return->role             = $user->role;

          if ($user->active == 1){
            $return->active = 'checked';
          }



        return response()->json($return);
      }else
      {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        else
        {
          $request->role;

          $role = Role::where('id',$request->role)->first();

          $user = User::findOrFail($id);
          $user->remember_token   = $request->_token;
          $user->active           = $request->active;
          $user->name             = $request->name;
          $user->email            = $request->email;
          $user->role             = $role->name;
          $user->role_id          = $role->id;


          $user->save();

          $return = new User;

          $return->id               = $user->id;
          $return->name             = $user->name;
          $return->email            = $user->email;
          $return->role             = $user->role;

            if ($user->active == 1){
              $return->active = 'checked';
            }
          }

          return response()->json($return);
      }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::findOrFail($id);
      $user->delete();

      return response()->json($user);
    }


    public function changeStatus(Request $request)
    {
        $id = Input::get('id');

        $user = User::findOrFail($id);

        $user->active = $request->active;

        $user->save();

        return response()->json($user);
    }
}
