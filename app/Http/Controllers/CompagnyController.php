<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Compagny;
use Illuminate\Http\Request;
use Validator;
use Response;
use Webpatser\Uuid\Uuid;
use view;

class CompagnyController extends Controller
{
    protected $rules = [
      '_token'        =>  'required',
      'namecompagny'  =>  'required',
      'firstname'     =>  'required',
      'lastname'      =>  'required',
      'email'         =>  'required',
      'streetaddress' =>  'required',
      'city'          =>  'required',
      'stateregion'   =>  'required',
      'country'       =>  'required',
      'codepostal'    =>  'required',
      'phonenumber'   =>  'required',
      'active'        =>  'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compagny = Compagny::all();

        return view('compagny',['compagny' => $compagny]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        else
          {

          $compagny = new Compagny;

          //$compagny->remember_token  = $request->_token;
          $compagny->nameCompagny   = $request->namecompagny;
          $compagny->firstname      = $request->firstname;
          $compagny->lastname       = $request->lastname;
          $compagny->email          = $request->email;
          $compagny->streetAddress  = $request->streetaddress;
          $compagny->city           = $request->city;
          $compagny->stateRegion    = $request->stateregion;
          $compagny->country        = $request->country;
          $compagny->codePostal     = $request->codepostal;
          $compagny->phoneNumber    = $request->phonenumber;
          $compagny->active         = $request->active;
          $compagny->uuidCompagnyPo = Uuid::generate(4);

          $compagny->save();

          $return = new Compagny;

          $return->id               = $compagny->id;
          $return->namecompagny     = $compagny->nameCompagny;
          $return->firstname        = $compagny->firstname;
          $return->lastname         = $compagny->lastname;
          $return->email            = $compagny->email;
          $return->streetAddress    = $compagny->streetAddress;
          $return->city             = $compagny->city;
          $return->stateRegion      = $compagny->stateRegion;
          $return->country          = $compagny->country;
          $return->codePostal       = $compagny->codePostal;
          $return->phoneNumber      = $compagny->phoneNumber;
          if ($compagny->active == 1){
            $return->active = 'checked';
          }


          return response()->json($return);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compagny  $compagny
     * @return \Illuminate\Http\Response
     */
    // public function show(Compagny $compagny)
    // {
    //
    //     return view('modalCompUpdate',
    //      ['compagny' => $compagny]);
    // }

    public function show(Compagny $compagny)
   {
       $id = Compagny::findOrFail($id);

       return view('compagny.show', ['id' => $id]);
   }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compagny  $compagny
     * @return \Illuminate\Http\Response
     */
    // public function edit(Compagny $compagny)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compagny  $compagny
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
          return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      }
      else
      {
        // if ($request->active == 'On')
        // {
        //   $active = 1;
        // }
        // else
        // {
        //   $active = 0;
        // }

        $compagny = new Compagny;

        $compagny->remember_token  = $request->_token;
        $compagny->nameCompagny   = $request->namecompagny;
        $compagny->firstname      = $request->firstname;
        $compagny->lastname       = $request->lastname;
        $compagny->email          = $request->email;
        $compagny->streetAddress  = $request->streetaddress;
        $compagny->city           = $request->city;
        $compagny->stateRegion    = $request->stateregion;
        $compagny->country        = $request->country;
        $compagny->codePostal     = $request->codepostal;
        $compagny->phoneNumber    = $request->phonenumber;
        $compagny->active         = $request->active;

        $compagny->save();

        $return = new Compagny;

        $return->id               = $compagny->id;
        $return->nameCompagny     = $compagny->nameCompagny;
        $return->firstname        = $compagny->firstname;
        $return->lastname         = $compagny->lastname;
        $return->email            = $compagny->email;

        return response()->json($return);

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compagny  $compagny
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $compagny = Compagny::findOrFail($id);
      $compagny->delete();

      return response()->json($compagny);
    }

    public function changeStatus(Request $request)
    {
        $id = Input::get('id');

        $compagny = Compagny::findOrFail($id);


        $compagny->active = $request->active;
        $compagny->remember_token = $request->_token;

        $compagny->save();

        return response()->json($compagny);
    }

}
