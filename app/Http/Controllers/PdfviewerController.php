<?php

namespace App\Http\Controllers;

use App\Pdfviewer;
use Illuminate\Http\Request;

class PdfviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pdfviewer');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pdfviewer  $pdfviewer
     * @return \Illuminate\Http\Response
     */
    public function show(Pdfviewer $pdfviewer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pdfviewer  $pdfviewer
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdfviewer $pdfviewer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pdfviewer  $pdfviewer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pdfviewer $pdfviewer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pdfviewer  $pdfviewer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pdfviewer $pdfviewer)
    {
        //
    }

    public function pdfViewer()
    {
      return view('pdfviewer');
    }
}
