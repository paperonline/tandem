@extends('layouts.dashboard')

@section('page_title')
	Task Management
@stop

@section('page_heading')
	 Task Management
@stop
@section('dashboard-content')


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>




    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>


			<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css" />

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


    <style>
        .panel-heading {
            padding: 0;
        }
        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .panel-heading li {
            float: left;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }
        /* .panel-heading li:last-child:hover {
            background-color: #ccc;
        }
        .panel-heading li:last-child {
            border-right: none;
        }
        .panel-heading li a:hover {
            text-decoration: none;
        } */

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }
        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }
    </style>

</head>

<body>

    <div class="col-sm-12">

        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
							<ul>
									<li><button type="button" class="btn btn-success btn-bordered add-modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;Task</button></li>
							</ul>
            </div>
            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                        <thead>
                            <tr>
                                <th valign="middle">ID</th>
                                <th>Code</th>
                                <th>Name</th>
																<th>Departement</th>
                                <th>Actions</th>
                            </tr>
                          {{ csrf_field() }}
                        </thead>
                        <tbody id="tbody">
													<?php $i=1;?>
                            @foreach($taches as $tache)

                                <tr class="item{{$tache->id}} @if($tache->is_published) warning @endif">
                                    <td>{{$tache->id}}</td>
                                    <td>{{$tache->code_tache}}</td>
                                    <td>{{$tache->nom_tache}}</td>
																		@foreach($departements as $departement)
																		@if($tache->departement_id == $departement->id)
																		<td id="td_depart<?=$i?>" data-td="{{$tache->departement_id}}">{{$departement->nom_departement}}</td>
																		@endif
																		@endforeach
                                    <td>

																				<button id="btnedit<?=$i?>" class="edit-modal btn btn-info"
																				data-id="{{$tache->id}}"
																				data-code="{{$tache->code_tache}}"
																				data-name="{{$tache->nom_tache}}"
																				data-depart="<?=$i?>">
                                        <span><i class="fa fa-edit"></i></span> Edit</button>

																				<button class="delete-modal btn btn-danger"
																				data-id="{{$tache->id}}"
																				data-code="{{$tache->code_tache}}"
																				data-name="{{$tache->nom_tache}}">
																				<span><i class="fa fa-trash-o"></i></span> Delete</button>
                                    </td>
                                </tr>
																<script>
																	$('#btnedit<?=$i?>').click(function(){
																		var td_depart = $('#td_depart<?=$i?>').text();
																		$('#departements_edit').each(function () {
												               $('#departements_edit option:selected').removeAttr('selected').trigger('change');
												            });
	                                  $("#departements_edit option[value="+td_depart+"").attr('selected', 'selected').trigger('change');
																	});
																</script>
														<?php $i++; ?>
                            @endforeach

                        </tbody>
                    </table>
										<ul class="nav nav-pills" style="display: flex;align-items: center;justify-content: right;">
											<li class="nav-item">
												{{ $taches->links() }}
											</li>
									</ul>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->




    <!-- Modal form to add a User -->
     <div id="addModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
												<div class="form-group">
                           <label for="code_add" class="col-sm-2 control-label" for="code">Code:</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="code_add" placeholder="Code" autofocus>
                           <p class="errorCode text-center alert alert-danger hidden">The Code field is required.</p>
                           </div>
                         </div>

                         <div class="form-group">
                           <label for="name_add" class="col-sm-2 control-label" for="name">Name:</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="name_add" placeholder="Name" autofocus>
                           <p class="errorName text-center alert alert-danger hidden">The Name field is required.</p>
                           </div>
                         </div>

												 <div class="form-group">
                          <label for="inputtext" class="col-sm-2 control-label">Departements</label>
                          <div class="col-sm-10">
                            <select class="form-control" id="departements_add">
													  @foreach($departements as $departement)
															<option value="{{$departement->id}}">{{$departement->nom_departement}}</option>
														@endforeach
                            </select>
                          </div>
                        </div>

                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-success add" data-dismiss="modal">
                             <span id=""><i class="fa fa-check"></i></span> Add
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to show a post -->
     <div id="showModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="title">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="content">Email:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="email_show" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to edit a form -->
     <div id="editModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_edit" disabled>
                             </div>
                         </div>

													<div class="form-group">
                             <label class="control-label col-sm-2" for="name">Code:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="code_edit" autofocus>
                                 <p class="errorCode text-center alert alert-danger hidden">The Code field is required.</p>
                             </div>
                         </div>

                         <div class="form-group">
                             <label class="control-label col-sm-2" for="name">Name:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="name_edit" autofocus>
                                 <p class="errorName text-center alert alert-danger hidden">The Name field is required.</p>
                             </div>
                         </div>

												 <div class="form-group">
                          <label for="inputtext" class="col-sm-2 control-label">Departements</label>
                          <div class="col-sm-10">
                            <select class="form-control" id="departements_edit">
													  @foreach($departements as $departement)
															<option value="{{$departement->nom_departement}}" id="id_depart{{$departement->id}}" data-id_depart="{{$departement->id}}">{{$departement->nom_departement}}</option>
														@endforeach
                            </select>
                          </div>
                        </div>
                  </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                             <span><i class="fa fa-check"></i></span> Edit
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
									</div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <h3 class="text-center">Are you sure you want to delete this user?</h3>
                     <br />
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_delete" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="name">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_delete" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                             <span id=""><i class="fa fa-thrash"></i></span> Delete
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>
@include('tacheAjax')

@stop
