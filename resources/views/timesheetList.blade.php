@extends('layouts.dashboard')

@section('page_title')
	Users Management
@stop
<!-- @section('page_subtitle')
Bootstrap UI Components
@stop -->
@section('page_heading')
	Company Management
@stop
@section('dashboard-content')


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>Manage Posts</title>


    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="/resources/demos/style.css">


    <style>
        .panel-heading {
            padding: 0;
        }
        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .panel-heading li {
            float: left;
            border-right:1px solid #bbb;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }
        /* .panel-heading li:last-child:hover {
            background-color: #ccc;
        }
        .panel-heading li:last-child {
            border-right: none;
        }
        .panel-heading li a:hover {
            text-decoration: none;
        } */

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }
        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }
    </style>

</head>

<body>
    <div class="col-sm-12">
        <h2 class="text-center">Feuille de temps</h2>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
							<ul>
									<li><button type="button" class="btn btn-success btn-bordered add-modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;Department</button></li>
							</ul>
            </div>
            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                        <thead>

                        </thead>
                        <tbody>

                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->




    <!-- Modal form to add a timesheet -->
     <div id="addModal" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg" style="width:100%;">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                   <div class="panel panel-default" style="border-color:#FFFFFF;">
                       <div class="panel-heading" style="background: #f8f5f500; border-color:#FFFFFF;">



                       </div>
                   <div class="panel-body">
										 <form class="form-inline" method="post" id="dynamic_form">
											 {{ csrf_field() }}

											 <span id="result"></span>

                           <table class="table table-striped table-bordered table-hover" id="user_table">
                               <thead>
																 	<tr>
																		<input type="text" id="jours" colspan="2" align="right" name="jour" readonly>
																		<div class="input-group date" data-provide="datepicker">
																			<input type="text" class="form-control" id="date_add" name="date_add">
																		    <div class="input-group-addon">
																		        <span class="glyphicon glyphicon-th"></span>
																		    </div>
																		</div>
																	</tr>
                                   <tr>
                                       <th valign="middle">Dossier</th>
                                       <th>Client</th>
                                       <th>Titre tache</th>
                                       <th>Debut</th>
                                       <th>Fin</th>
                                       <th>Temps</th>
                                       <th>Departement</th>
                                       <th>Tache</th>
                                       <th>Commentaire</th>
																			 <th>Action</th>
                                   </tr>

                               </thead>

                               <tbody id="addForm">

															 </tbody>

                            </table>
														<div class="modal-footer">
 														@csrf
			 											 <input type="submit" name="save" id="save" class="btn btn-primary" value="Save"/>
			                          <button type="button" class="btn btn-warning" data-dismiss="modal">
			                              <span><i class="fa fa-remove"></i></span> Close
			                          </button>
			                      </div>
														</form>
                         </div>
                      </div>
                 </div>
             </div>
         </div>
     </div>



		     <!-- Modal form to edit a timesheet -->
		      <div id="editModal" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg" style="width:100%;">
		              <div class="modal-content">
		                  <div class="modal-header">
		                      <button type="button" class="close" data-dismiss="modal">×</button>
		                      <h4 class="modal-title"></h4>
		                  </div>
		                  <div class="modal-body">
		                    <div class="panel panel-default" style="border-color:#FFFFFF;">
		                        <div class="panel-heading" style="background: #f8f5f500; border-color:#FFFFFF;">


													</div>
		                    <div class="panel-body">
		 										 <form class="form-inline" method="post" id="dynamic_form">
		 											 {{ csrf_field() }}
		 											 <span id="result"></span>
													 	<p id="demo"></p>
		                            <table class="table table-striped table-bordered table-hover" id="user_table">
		                                <thead>
		 																 	<tr>
		 																		<input type="text" id="jours" colspan="2" align="right" name="jour" readonly>
		 																		<div class="input-group date" data-provide="datepicker">
		 																			<input type="text" class="form-control" id="date_add" name="date_add">
		 																		    <div class="input-group-addon">
		 																		        <span class="glyphicon glyphicon-th"></span>
		 																		    </div>
		 																		</div>
		 																	</tr>
		                                    <tr>
		                                        <th valign="middle">Dossier</th>
		                                        <th>Client</th>
		                                        <th>Titre tache</th>
		                                        <th>Debut</th>
		                                        <th>Fin</th>
		                                        <th>Temps</th>
		                                        <th>Departement</th>
		                                        <th>Tache</th>
		                                        <th>Commentaire</th>
		 																			 <th>Action</th>
		                                    </tr>

		                                </thead>

		                                <tbody id="editForm">
																			<td><input id="id_edit" type="hidden" value"">
		 															 </tbody>

		                             </table>
		 														<div class="modal-footer">
		  														@csrf
		 			 											 <input type="submit" name="save" id="save" class="btn btn-primary" value="Save"/>
		 			                          <button type="button" class="btn btn-warning" data-dismiss="modal">
		 			                              <span><i class="fa fa-remove"></i></span> Close
		 			                          </button>
		 			                      </div>
		 														</form>
		                          </div>
		                       </div>
		                  </div>
		              </div>
		          </div>
		      </div>

		 <script type="text/javascript">
		 $(document).ready(function(){
				$( function() {
				     $( "#date_add" ).datepicker();
						 $('#date_add').on('changeDate', function() {
 								$('#jours').val(
 										$('#date_add').datepicker('getFormattedDate')
 								);
 						});
				   } );

			  });
	 	 </script>



     <!-- Modal form to show a post -->
     <div id="showModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="title">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="content">Email:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="email_show" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>


     <!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <h3 class="text-center">Are you sure you want to delete this user?</h3>
                     <br />
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_delete" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="name">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_delete" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                             <span id=""><i class="fa fa-thrash"></i></span> Delete
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>
{{-- @include('timesheetAjax') --}}

@stop
