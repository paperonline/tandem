<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<!-- Bootstrap JavaScript -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

<!-- toastr notifications -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- icheck checkboxes -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js"></script>

<!-- Delay table load until everything else is loaded -->
<script>
    $(window).load(function(){
        $('#postTable').removeAttr('style');
    })
</script>


<!-- AJAX CRUD operations -->
<script type="text/javascript">
      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
    // add a new post
    $(document).on('click', '.add-modal', function() {
        $('.modal-title').text('Add');
        $('#addModal').modal('show');

    });

    $(document).ready(function(){
              // $('.published').iCheck({
              //     checkboxClass: 'icheckbox_square-yellow',
              //     radioClass: 'iradio_square-yellow',
              //     increaseArea: '20%'
              // });
              $('.published').on('click', function(event){
                  id = $(this).data('id');
                  if($(this).is(":checked")) {
                        var active = 1;
                    } else {
                        var active = 0;
                    }
                  $.ajax({
                      type: 'POST',
                      url: "userController/changeStatus",
                      data: {
                          '_token': $('input[name=_token]').val(),
                          'id': id,
                          'active': active
                      },
                      success: function(data) {
                          // empty
                      },
                  });
              });
              $('.published').on('ifToggled', function(event) {
                  $(this).closest('tr').toggleClass('warning');
              });
          });



    $('.modal-footer').on('click', '.add', function() {

      if($('#switch_add').is(":checked")) {

            var active = 1;
            var checked = 'checked';
        } else {
            var active = 0;
            var checked = '';
        }

        $.ajax({
            type: 'POST',
            url: "{{ route('userController.store')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'active': active,
                // 'compagny': $('#compagny_add').val(),
                'name': $('#name_add').val(),
                'email': $('#email_add').val(),
                'password': $('#password_add').val(),
                'confirmpassword': $('#confirmpassword_add').val(),
                'role': $('#role_add').val(),

            },
            success: function(data) {

                $('.errorTitle').addClass('hidden');
                $('.errorContent').addClass('hidden');

                if ((data.errors)) {
                    setTimeout(function () {
                        $('#addModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                    }, 500);

                    if (data.errors.name) {
                        $('.errorName').removeClass('hidden');
                        $('.errorName').text(data.errors.name);
                    }
                    if (data.errors.email) {
                        $('.errorEmail').removeClass('hidden');
                        $('.errorEmail').text(data.errors.email);
                    }
                    if (data.errors.password) {
                        $('.errorPassword').removeClass('hidden');
                        $('.errorPassword').text(data.errors.password);
                    }
                    if (data.errors.confirmpassword) {
                        $('.errorConfirmpassword').removeClass('hidden');
                        $('.errorConfirmpassword').text(data.errors.confirmpassword);
                    }
                } else {

                    toastr.success('Successfully added!', 'Success Alert', {timeOut: 5000});
                    $('#postTable').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td>" + data.role + "</td><td class='text-center'><input type='checkbox' id='active_add' class='published' "+checked+"></td><td>Right now</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");

                }
            },
        });
    });
    // Show a post
    $(document).on('click', '.show-modal', function() {
        $('.modal-title').text('Show');
        $('#id_show').val($(this).data('id'));
        $('#name_show').val($(this).data('name'));
        $('#email_show').val($(this).data('email'));
        $('#showModal').modal('show');
    });

var i =0;
    // Edit a post
    $(document).on('click', '.edit-modal', function() {

        $('.modal-title').text('Edit');
        $('#id_edit').val($(this).data('id'));
        $('#name_edit').val($(this).data('name'));
        $('#email_edit').val($(this).data('email'));
        $('#active_edit').val($(this).data('active'));
        $('#role_edit').val($(this).data('role'));
        id = $('#id_edit').val();
        if($('#userActive'+id+'').is(":checked")) {
            var checked = true;
          } else {
             var checked = false;
          }
        $('#switch_edit').prop("checked", checked);
        $('#editModal').modal('show');
        //$('#switch_edit').remplaceWith('<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_edit"> checked="1"');
    });

    $('.modal-footer').on('click', '.edit', function() {

            if($('#switch_edit').is(":checked")) {
                var active = 1;
                var checked = 'checked';
            } else {
                var active = 0;
                var checked = '';
            }

        $.ajax({
            type: 'PUT',
            url: 'userController/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#id_edit").val(),
                'active': active,
                // 'compagny': $('#compagny_edit').val(),
                'name': $('#name_edit').val(),
                'email': $('#email_edit').val(),
                'password': $('#password_edit').val(),
                'confirmpassword': $('#confirmpassword_edit').val(),
                'role': $('#role_edit').val(),
            },
            success: function(data) {
                $('.errorName').addClass('hidden');
                $('.errorEmail').addClass('hidden');

                if ((data.errors)) {
                    setTimeout(function () {
                        $('#editModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                    }, 500);

                    if (data.errors.name) {
                        $('.errorName').removeClass('hidden');
                        $('.errorName').text(data.errors.name);
                    }
                    if (data.errors.email) {
                        $('.errorEmail').removeClass('hidden');
                        $('.errorEmail').text(data.errors.email);
                    }
                    if (data.errors.password) {
                        $('.errorPassword').removeClass('hidden');
                        $('.errorPassword').text(data.errors.password);
                    }
                    if (data.errors.confirmpassword) {
                        $('.errorConfirmpassword').removeClass('hidden');
                        $('.errorConfirmpassword').text(data.errors.confirmpassword);
                    }
                } else {
                    toastr.success('Successfully updated!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td>" + data.role + "</td><td class='text-center'><input type='checkbox' id='active_add' class='published' "+data.active+"></td><td>Right now</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");
                }
            }
        });
    });

    // delete a post
    $(document).on('click', '.delete-modal', function() {
        $('.modal-title').text('Delete');
        $('#id_delete').val($(this).data('id'));
        $('#name_delete').val($(this).data('name'));
        $('#deleteModal').modal('show');
        id = $('#id_delete').val();
    });
    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'DELETE',
            url: 'userController/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 5000});
                $('.item' + data['id']).remove();
            }
        });
    });
</script>

</body>
</html>
