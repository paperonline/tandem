@extends('layouts.dashboard')

@section('page_title')
	Company Management
@stop
@section('page_heading')
	Company Management
@stop
@section('dashboard-content')

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>

  


    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .panel-heading {
            padding: 0;
        }
        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .panel-heading li {
            float: left;
            border-right:1px solid #bbb;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }
        .panel-heading li:last-child:hover {
            background-color: #ccc;
        }
        .panel-heading li:last-child {
            border-right: none;
        }
        .panel-heading li a:hover {
            text-decoration: none;
        }

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }
        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }

    </style>

</head>

<body>
    <div class="col-sm-12">
        <h2 class="text-center">Manage Compagny</h2>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <ul>
                    <a href="#" class="add-modal"><li>Add a Post</li></a>
                </ul>
            </div>

            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                        <thead>
                            <tr>
                                <th valign="middle">ID</th>
                                <th>Compagny</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
																<th>Active</th>
                                <th>Last updated</th>
                                <th>Actions</th>
                            </tr>
                          {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($compagny as $comp)

                                <tr class="item{{$comp->id}} @if($comp->is_published) warning @endif">
                                    <td>{{$comp->id}}</td>
																		<td>{{$comp->nameCompagny}}</td>
                                    <td>{{$comp->firstname}}</td>
                                    <td>{{$comp->lastname}}</td>
                                    <td>{{$comp->email}}</td>
																		<td class="text-center"><input type="checkbox" class="published" id="compActive{{$comp->id}}" data-id="{{$comp->id}}" @if ($comp->active == 1) checked @endif></td>
                                    <td>
                                      {{-- {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comp->updated_at)->diffForHumans() }} --}}
                                    </td>
                                    <td>
                                        <button class="show-modal btn btn-success"
																					data-id							=	"{{$comp->id}}"
																					data-active					=	"{{$comp->active}}"
																					data-namecompagny		=	"{{$comp->nameCompagny}}"
																					data-firstname			=	"{{$comp->firstname}}"
																					data-lastname				=	"{{$comp->lastname}}"
																					data-email					=	"{{$comp->email}}"
																					data-streetaddress	=	"{{$comp->streetAddress}}"
																					data-city						=	"{{$comp->city}}"
																					data-stateregion		=	"{{$comp->stateRegion}}"
																					data-country				=	"{{$comp->country}}"
																					data-codepostal			=	"{{$comp->codePostal}}"
																					data-phonenumber		=	"{{$comp->phoneNumber}}">
                                        <span><i class="fa fa-eye"></i></span>
																				Show</button>

                                        <button class="edit-modal btn btn-info"
																				data-id							=	"{{$comp->id}}"
																				data-active					=	"{{$comp->active}}"
																				data-namecompagny		=	"{{$comp->nameCompagny}}"
																				data-firstname			=	"{{$comp->firstname}}"
																				data-lastname				=	"{{$comp->lastname}}"
																				data-email					=	"{{$comp->email}}"
																				data-streetaddress	=	"{{$comp->streetAddress}}"
																				data-city						=	"{{$comp->city}}"
																				data-stateregion		=	"{{$comp->stateRegion}}"
																				data-country				=	"{{$comp->country}}"
																				data-codepostal			=	"{{$comp->codePostal}}"
																				data-phonenumber		=	"{{$comp->phoneNumber}}">
                                        <span><i class="fa fa-edit"></i></span>
																				Edit</button>

                                        <button class="delete-modal btn btn-danger"
																				data-id							=	"{{$comp->id}}"
																				data-active					=	"{{$comp->active}}"
																				data-namecompagny		=	"{{$comp->nameCompagny}}"
																				data-firstname			=	"{{$comp->firstname}}"
																				data-lastname				=	"{{$comp->lastname}}"
																				data-email					=	"{{$comp->email}}"
																				data-streetaddress	=	"{{$comp->streetAddress}}"
																				data-city						=	"{{$comp->city}}"
																				data-stateregion		=	"{{$comp->stateRegion}}"
																				data-country				=	"{{$comp->country}}"
																				data-codepostal			=	"{{$comp->codePostal}}"
																				data-phonenumber		=	"{{$comp->phoneNumber}}">
                                        <span><i class="fa fa-trash-o"></i></span>
																				Delete</button>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->




    <!-- Modal form to add a User -->
     <div id="addModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">

                           <div class="form-group">
                             <label for="active_add" class="col-sm-2 control-label" for="name">Active</label>
                             <input type="checkbox" id="active_add" class="published" data-id="{{$comp->id}}" @if ($comp->active == 1) checked @endif>
                          </div>

													<div class="form-group">
                           <label for="namecompagny_add" class="col-sm-2 control-label" for="namecompagny">Compagny</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="namecompagny_add" placeholder="Name of Compagny" autofocus>
                           <p class="errornamecompagny text-center alert alert-danger hidden">The Compagny field is required.</p>
                           </div>
                         </div>

                          <div class="form-group">
                           <label for="firstname_add" class="col-sm-2 control-label" for="firstname">First Name</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="firstname_add" placeholder="First Name" autofocus>
                           <p class="errorfirstname text-center alert alert-danger hidden">The First Name field is required.</p>
                           </div>
                         	</div>

												 <div class="form-group">
													<label for="lastname_add" class="col-sm-2 control-label" for="lastname">Last Name</label>
													<div class="col-sm-10">
													<input type="text" class="form-control underline" id="lastname_add" placeholder="Last Name" autofocus>
													<p class="errorlastname text-center alert alert-danger hidden">The Last Name field is required.</p>
													</div>
												</div>

                         <div class="form-group">
                           <label for="email_add" class="col-sm-2 control-label" for="email">Email</label>
                           <div class="col-sm-10">
                           <input type="email" class="form-control underline" id="email_add" placeholder="Email" autofocus>
                           <p class="erroremail text-center alert alert-danger hidden">The Email field is required.</p>
                           </div>
                         </div>

                         <div class="form-group">
                           <label for="streetaddress_add" class="col-sm-2 control-label" for="streetaddress">Street Address</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="streetaddress_add" placeholder="Street Address" autofocus>
                           <p class="errorstreetaddress text-center alert alert-danger hidden">The Street Address field is required.</p>
                           </div>
                         </div>

                         <div class="form-group">
                           <label for="city_add" class="col-sm-2 control-label" for="city">City</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="city_add" placeholder="City" autofocus>
                           <p class="errorcity text-center alert alert-danger hidden">The City field is required.</p>
                           </div>
                         </div>

												 <div class="form-group">
                           <label for="codePostal_add" class="col-sm-2 control-label" for="codePostal">Code Postal</label>
                           <div class="col-sm-10">
                           <input type="text" class="form-control underline" id="codepostal_add" placeholder="Code Postal" autofocus>
                           <p class="errorcodePostal text-center alert alert-danger hidden">The Code Postal field is required.</p>
                           </div>
                         </div>

												 <div class="form-group">
                          <label for="inputtext" class="col-sm-2 control-label">Country</label>
                          <div class="col-sm-10">
														<select class="form-control" id="country_add">
															<option selected >Choose</option>
															<option value="AF">Afghanistan</option>
															<option value="AX">Åland Islands</option>
															<option value="AL">Albania</option>
															<option value="DZ">Algeria</option>
															<option value="AS">American Samoa</option>
															<option value="AD">Andorra</option>
															<option value="AO">Angola</option>
															<option value="AI">Anguilla</option>
															<option value="AQ">Antarctica</option>
															<option value="AG">Antigua and Barbuda</option>
															<option value="AR">Argentina</option>
															<option value="AM">Armenia</option>
															<option value="AW">Aruba</option>
															<option value="AU">Australia</option>
															<option value="AT">Austria</option>
															<option value="AZ">Azerbaijan</option>
															<option value="BS">Bahamas</option>
															<option value="BH">Bahrain</option>
															<option value="BD">Bangladesh</option>
															<option value="BB">Barbados</option>
															<option value="BY">Belarus</option>
															<option value="BE">Belgium</option>
															<option value="BZ">Belize</option>
															<option value="BJ">Benin</option>
															<option value="BM">Bermuda</option>
															<option value="BT">Bhutan</option>
															<option value="BO">Bolivia, Plurinational State of</option>
															<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
															<option value="BA">Bosnia and Herzegovina</option>
															<option value="BW">Botswana</option>
															<option value="BV">Bouvet Island</option>
															<option value="BR">Brazil</option>
															<option value="IO">British Indian Ocean Territory</option>
															<option value="BN">Brunei Darussalam</option>
															<option value="BG">Bulgaria</option>
															<option value="BF">Burkina Faso</option>
															<option value="BI">Burundi</option>
															<option value="KH">Cambodia</option>
															<option value="CM">Cameroon</option>
															<option value="CA">Canada</option>
															<option value="CV">Cape Verde</option>
															<option value="KY">Cayman Islands</option>
															<option value="CF">Central African Republic</option>
															<option value="TD">Chad</option>
															<option value="CL">Chile</option>
															<option value="CN">China</option>
															<option value="CX">Christmas Island</option>
															<option value="CC">Cocos (Keeling) Islands</option>
															<option value="CO">Colombia</option>
															<option value="KM">Comoros</option>
															<option value="CG">Congo</option>
															<option value="CD">Congo, the Democratic Republic of the</option>
															<option value="CK">Cook Islands</option>
															<option value="CR">Costa Rica</option>
															<option value="CI">Côte d'Ivoire</option>
															<option value="HR">Croatia</option>
															<option value="CU">Cuba</option>
															<option value="CW">Curaçao</option>
															<option value="CY">Cyprus</option>
															<option value="CZ">Czech Republic</option>
															<option value="DK">Denmark</option>
															<option value="DJ">Djibouti</option>
															<option value="DM">Dominica</option>
															<option value="DO">Dominican Republic</option>
															<option value="EC">Ecuador</option>
															<option value="EG">Egypt</option>
															<option value="SV">El Salvador</option>
															<option value="GQ">Equatorial Guinea</option>
															<option value="ER">Eritrea</option>
															<option value="EE">Estonia</option>
															<option value="ET">Ethiopia</option>
															<option value="FK">Falkland Islands (Malvinas)</option>
															<option value="FO">Faroe Islands</option>
															<option value="FJ">Fiji</option>
															<option value="FI">Finland</option>
															<option value="FR">France</option>
															<option value="GF">French Guiana</option>
															<option value="PF">French Polynesia</option>
															<option value="TF">French Southern Territories</option>
															<option value="GA">Gabon</option>
															<option value="GM">Gambia</option>
															<option value="GE">Georgia</option>
															<option value="DE">Germany</option>
															<option value="GH">Ghana</option>
															<option value="GI">Gibraltar</option>
															<option value="GR">Greece</option>
															<option value="GL">Greenland</option>
															<option value="GD">Grenada</option>
															<option value="GP">Guadeloupe</option>
															<option value="GU">Guam</option>
															<option value="GT">Guatemala</option>
															<option value="GG">Guernsey</option>
															<option value="GN">Guinea</option>
															<option value="GW">Guinea-Bissau</option>
															<option value="GY">Guyana</option>
															<option value="HT">Haiti</option>
															<option value="HM">Heard Island and McDonald Islands</option>
															<option value="VA">Holy See (Vatican City State)</option>
															<option value="HN">Honduras</option>
															<option value="HK">Hong Kong</option>
															<option value="HU">Hungary</option>
															<option value="IS">Iceland</option>
															<option value="IN">India</option>
															<option value="ID">Indonesia</option>
															<option value="IR">Iran, Islamic Republic of</option>
															<option value="IQ">Iraq</option>
															<option value="IE">Ireland</option>
															<option value="IM">Isle of Man</option>
															<option value="IL">Israel</option>
															<option value="IT">Italy</option>
															<option value="JM">Jamaica</option>
															<option value="JP">Japan</option>
															<option value="JE">Jersey</option>
															<option value="JO">Jordan</option>
															<option value="KZ">Kazakhstan</option>
															<option value="KE">Kenya</option>
															<option value="KI">Kiribati</option>
															<option value="KP">Korea, Democratic People's Republic of</option>
															<option value="KR">Korea, Republic of</option>
															<option value="KW">Kuwait</option>
															<option value="KG">Kyrgyzstan</option>
															<option value="LA">Lao People's Democratic Republic</option>
															<option value="LV">Latvia</option>
															<option value="LB">Lebanon</option>
															<option value="LS">Lesotho</option>
															<option value="LR">Liberia</option>
															<option value="LY">Libya</option>
															<option value="LI">Liechtenstein</option>
															<option value="LT">Lithuania</option>
															<option value="LU">Luxembourg</option>
															<option value="MO">Macao</option>
															<option value="MK">Macedonia, the former Yugoslav Republic of</option>
															<option value="MG">Madagascar</option>
															<option value="MW">Malawi</option>
															<option value="MY">Malaysia</option>
															<option value="MV">Maldives</option>
															<option value="ML">Mali</option>
															<option value="MT">Malta</option>
															<option value="MH">Marshall Islands</option>
															<option value="MQ">Martinique</option>
															<option value="MR">Mauritania</option>
															<option value="MU">Mauritius</option>
															<option value="YT">Mayotte</option>
															<option value="MX">Mexico</option>
															<option value="FM">Micronesia, Federated States of</option>
															<option value="MD">Moldova, Republic of</option>
															<option value="MC">Monaco</option>
															<option value="MN">Mongolia</option>
															<option value="ME">Montenegro</option>
															<option value="MS">Montserrat</option>
															<option value="MA">Morocco</option>
															<option value="MZ">Mozambique</option>
															<option value="MM">Myanmar</option>
															<option value="NA">Namibia</option>
															<option value="NR">Nauru</option>
															<option value="NP">Nepal</option>
															<option value="NL">Netherlands</option>
															<option value="NC">New Caledonia</option>
															<option value="NZ">New Zealand</option>
															<option value="NI">Nicaragua</option>
															<option value="NE">Niger</option>
															<option value="NG">Nigeria</option>
															<option value="NU">Niue</option>
															<option value="NF">Norfolk Island</option>
															<option value="MP">Northern Mariana Islands</option>
															<option value="NO">Norway</option>
															<option value="OM">Oman</option>
															<option value="PK">Pakistan</option>
															<option value="PW">Palau</option>
															<option value="PS">Palestinian Territory, Occupied</option>
															<option value="PA">Panama</option>
															<option value="PG">Papua New Guinea</option>
															<option value="PY">Paraguay</option>
															<option value="PE">Peru</option>
															<option value="PH">Philippines</option>
															<option value="PN">Pitcairn</option>
															<option value="PL">Poland</option>
															<option value="PT">Portugal</option>
															<option value="PR">Puerto Rico</option>
															<option value="QA">Qatar</option>
															<option value="RE">Réunion</option>
															<option value="RO">Romania</option>
															<option value="RU">Russian Federation</option>
															<option value="RW">Rwanda</option>
															<option value="BL">Saint Barthélemy</option>
															<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
															<option value="KN">Saint Kitts and Nevis</option>
															<option value="LC">Saint Lucia</option>
															<option value="MF">Saint Martin (French part)</option>
															<option value="PM">Saint Pierre and Miquelon</option>
															<option value="VC">Saint Vincent and the Grenadines</option>
															<option value="WS">Samoa</option>
															<option value="SM">San Marino</option>
															<option value="ST">Sao Tome and Principe</option>
															<option value="SA">Saudi Arabia</option>
															<option value="SN">Senegal</option>
															<option value="RS">Serbia</option>
															<option value="SC">Seychelles</option>
															<option value="SL">Sierra Leone</option>
															<option value="SG">Singapore</option>
															<option value="SX">Sint Maarten (Dutch part)</option>
															<option value="SK">Slovakia</option>
															<option value="SI">Slovenia</option>
															<option value="SB">Solomon Islands</option>
															<option value="SO">Somalia</option>
															<option value="ZA">South Africa</option>
															<option value="GS">South Georgia and the South Sandwich Islands</option>
															<option value="SS">South Sudan</option>
															<option value="ES">Spain</option>
															<option value="LK">Sri Lanka</option>
															<option value="SD">Sudan</option>
															<option value="SR">Suriname</option>
															<option value="SJ">Svalbard and Jan Mayen</option>
															<option value="SZ">Swaziland</option>
															<option value="SE">Sweden</option>
															<option value="CH">Switzerland</option>
															<option value="SY">Syrian Arab Republic</option>
															<option value="TW">Taiwan, Province of China</option>
															<option value="TJ">Tajikistan</option>
															<option value="TZ">Tanzania, United Republic of</option>
															<option value="TH">Thailand</option>
															<option value="TL">Timor-Leste</option>
															<option value="TG">Togo</option>
															<option value="TK">Tokelau</option>
															<option value="TO">Tonga</option>
															<option value="TT">Trinidad and Tobago</option>
															<option value="TN">Tunisia</option>
															<option value="TR">Turkey</option>
															<option value="TM">Turkmenistan</option>
															<option value="TC">Turks and Caicos Islands</option>
															<option value="TV">Tuvalu</option>
															<option value="UG">Uganda</option>
															<option value="UA">Ukraine</option>
															<option value="AE">United Arab Emirates</option>
															<option value="GB">United Kingdom</option>
															<option value="US">United States</option>
															<option value="UM">United States Minor Outlying Islands</option>
															<option value="UY">Uruguay</option>
															<option value="UZ">Uzbekistan</option>
															<option value="VU">Vanuatu</option>
															<option value="VE">Venezuela, Bolivarian Republic of</option>
															<option value="VN">Viet Nam</option>
															<option value="VG">Virgin Islands, British</option>
															<option value="VI">Virgin Islands, U.S.</option>
															<option value="WF">Wallis and Futuna</option>
															<option value="EH">Western Sahara</option>
															<option value="YE">Yemen</option>
															<option value="ZM">Zambia</option>
															<option value="ZW">Zimbabwe</option>
														</select>
                          </div>
                        </div>

												<div class="form-group">
													<label for="stateregion_add" class="col-sm-2 control-label" for="stateregion">State / Region</label>
													<div class="col-sm-10">
													<input type="text" class="form-control underline" id="stateregion_add" placeholder="State / Region" autofocus>
													<p class="errorstateregion text-center alert alert-danger hidden">The State or Region field is required.</p>
													</div>
												</div>

												<div class="form-group">
													<label for="phonenumber_add" class="col-sm-2 control-label" for="phonenumber">Phone Number</label>
													<div class="col-sm-10">
													<input type="text" class="form-control underline" id="phonenumber_add" placeholder="Phone Number" autofocus>
													<p class="errorphonenumber text-center alert alert-danger hidden">The Phone Number field is required.</p>
													</div>
												</div>

                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-success add" data-dismiss="modal">
                             <span id=""><i class="fa fa-check"></i></span> Add
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to show a post -->
     <div id="showModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control underline" id="id_show" disabled>
                             </div>
                         </div>
												 <div class="form-group">
                             <label class="control-label col-sm-2" for="id">Active:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control underline" id="active_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="nameCompagny">Compagny:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control underline" id="namecompagny_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="content">First Name:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control underline" id="firstname_show" disabled>
                             </div>
                         </div>
												 <div class="form-group">
                             <label class="control-label col-sm-2" for="lastname">Last Name:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control underline" id="lastname_show" disabled>
                             </div>
                         </div>
												 <div class="form-group">
                             <label class="control-label col-sm-2" for="email">Email:</label>
                             <div class="col-sm-10">
                                 <input type="email" class="form-control underline" id="email_show" disabled>
                             </div>
                         </div>
												 <div class="form-group">
														 <label class="control-label col-sm-2" for="streetAddress">Street Address:</label>
														 <div class="col-sm-10">
																 <input type="text" class="form-control underline" id="streetaddress_show" disabled>
														 </div>
												 </div>
												 <div class="form-group">
														 <label class="control-label col-sm-2" for="city">City:</label>
														 <div class="col-sm-10">
																 <input type="text" class="form-control underline" id="city_show" disabled>
														 </div>
												 </div>
												 <div class="form-group">
														 <label class="control-label col-sm-2" for="stateregion">State / Region:</label>
														 <div class="col-sm-10">
																 <input type="text" class="form-control underline" id="stateregion_show" disabled>
														 </div>
												 </div>
												 <div class="form-group">
														 <label class="control-label col-sm-2" for="country">Country:</label>
														 <div class="col-sm-10">
																 <input type="text" class="form-control underline" id="country_show" disabled>
														 </div>
												 </div>
												 <div class="form-group">
														 <label class="control-label col-sm-2" for="phonenumber">Phone Number:</label>
														 <div class="col-sm-10">
																 <input type="text" class="form-control underline" id="phonenumber_show" disabled>
														 </div>
												 </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to edit a form -->
     <div id="editModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">

                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_edit" disabled>
                             </div>
                         </div>

												 <div class="form-group">
 													<label for="active_edit" class="col-sm-2 control-label" for="name">Active</label>
 													<div class="onoffswitch">
 													 <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_edit" checked="1">
 													 <label class="onoffswitch-label" for="switch3">
 													 <span class="onoffswitch-inner"></span>
 													 <span class="onoffswitch-switch"></span>
 													 </label>
 												 </div>
 											 </div>

 											 <div class="form-group">
 												<label for="namecompagny_edit" class="col-sm-2 control-label" for="namecompagny">Compagny</label>
 												<div class="col-sm-10">
 												<input type="text" class="form-control underline" id="namecompagny_edit" placeholder="Name of Compagny" autofocus>
 												<p class="errornamecompagny text-center alert alert-danger hidden">The Compagny field is required.</p>
 												</div>
 											</div>

 											 <div class="form-group">
 												<label for="firstname_edit" class="col-sm-2 control-label" for="firstname">First Name</label>
 												<div class="col-sm-10">
 												<input type="text" class="form-control underline" id="firstname_edit" placeholder="First Name" autofocus>
 												<p class="errorfirstname text-center alert alert-danger hidden">The First Name field is required.</p>
 												</div>
 											 </div>

 											<div class="form-group">
 											 <label for="lastname_edit" class="col-sm-2 control-label" for="lastname">Last Name</label>
 											 <div class="col-sm-10">
 											 <input type="text" class="form-control underline" id="lastname_edit" placeholder="Last Name" autofocus>
 											 <p class="errorlastname text-center alert alert-danger hidden">The Last Name field is required.</p>
 											 </div>
 										 </div>

 											<div class="form-group">
 												<label for="email_edit" class="col-sm-2 control-label" for="email">Email</label>
 												<div class="col-sm-10">
 												<input type="email" class="form-control underline" id="email_edit" placeholder="Email" autofocus>
 												<p class="erroremail text-center alert alert-danger hidden">The Email field is required.</p>
 												</div>
 											</div>

 											<div class="form-group">
 												<label for="streetaddress_edit" class="col-sm-2 control-label" for="streetaddress">Street Address</label>
 												<div class="col-sm-10">
 												<input type="text" class="form-control underline" id="streetaddress_edit" placeholder="Street Address" autofocus>
 												<p class="errorstreetaddress text-center alert alert-danger hidden">The Street Address field is required.</p>
 												</div>
 											</div>

 											<div class="form-group">
 												<label for="city_edit" class="col-sm-2 control-label" for="city">City</label>
 												<div class="col-sm-10">
 												<input type="text" class="form-control underline" id="city_edit" placeholder="City" autofocus>
 												<p class="errorcity text-center alert alert-danger hidden">The City field is required.</p>
 												</div>
 											</div>

 											<div class="form-group">
 												<label for="codePostal_edit" class="col-sm-2 control-label" for="codePostal">Code Postal</label>
 												<div class="col-sm-10">
 												<input type="text" class="form-control underline" id="codePostal_edit" placeholder="Code Postal" autofocus>
 												<p class="errorcodePostal text-center alert alert-danger hidden">The Code Postal field is required.</p>
 												</div>
 											</div>

 											<div class="form-group">
 											 <label for="inputtext" class="col-sm-2 control-label">Country</label>
 											 <div class="col-sm-10">
 												 <select class="form-control" id="country_edit">
 													 <option selected >Choose</option>
 													 <option value="AF">Afghanistan</option>
 													 <option value="AX">Åland Islands</option>
 													 <option value="AL">Albania</option>
 													 <option value="DZ">Algeria</option>
 													 <option value="AS">American Samoa</option>
 													 <option value="AD">Andorra</option>
 													 <option value="AO">Angola</option>
 													 <option value="AI">Anguilla</option>
 													 <option value="AQ">Antarctica</option>
 													 <option value="AG">Antigua and Barbuda</option>
 													 <option value="AR">Argentina</option>
 													 <option value="AM">Armenia</option>
 													 <option value="AW">Aruba</option>
 													 <option value="AU">Australia</option>
 													 <option value="AT">Austria</option>
 													 <option value="AZ">Azerbaijan</option>
 													 <option value="BS">Bahamas</option>
 													 <option value="BH">Bahrain</option>
 													 <option value="BD">Bangladesh</option>
 													 <option value="BB">Barbados</option>
 													 <option value="BY">Belarus</option>
 													 <option value="BE">Belgium</option>
 													 <option value="BZ">Belize</option>
 													 <option value="BJ">Benin</option>
 													 <option value="BM">Bermuda</option>
 													 <option value="BT">Bhutan</option>
 													 <option value="BO">Bolivia, Plurinational State of</option>
 													 <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
 													 <option value="BA">Bosnia and Herzegovina</option>
 													 <option value="BW">Botswana</option>
 													 <option value="BV">Bouvet Island</option>
 													 <option value="BR">Brazil</option>
 													 <option value="IO">British Indian Ocean Territory</option>
 													 <option value="BN">Brunei Darussalam</option>
 													 <option value="BG">Bulgaria</option>
 													 <option value="BF">Burkina Faso</option>
 													 <option value="BI">Burundi</option>
 													 <option value="KH">Cambodia</option>
 													 <option value="CM">Cameroon</option>
 													 <option value="CA">Canada</option>
 													 <option value="CV">Cape Verde</option>
 													 <option value="KY">Cayman Islands</option>
 													 <option value="CF">Central African Republic</option>
 													 <option value="TD">Chad</option>
 													 <option value="CL">Chile</option>
 													 <option value="CN">China</option>
 													 <option value="CX">Christmas Island</option>
 													 <option value="CC">Cocos (Keeling) Islands</option>
 													 <option value="CO">Colombia</option>
 													 <option value="KM">Comoros</option>
 													 <option value="CG">Congo</option>
 													 <option value="CD">Congo, the Democratic Republic of the</option>
 													 <option value="CK">Cook Islands</option>
 													 <option value="CR">Costa Rica</option>
 													 <option value="CI">Côte d'Ivoire</option>
 													 <option value="HR">Croatia</option>
 													 <option value="CU">Cuba</option>
 													 <option value="CW">Curaçao</option>
 													 <option value="CY">Cyprus</option>
 													 <option value="CZ">Czech Republic</option>
 													 <option value="DK">Denmark</option>
 													 <option value="DJ">Djibouti</option>
 													 <option value="DM">Dominica</option>
 													 <option value="DO">Dominican Republic</option>
 													 <option value="EC">Ecuador</option>
 													 <option value="EG">Egypt</option>
 													 <option value="SV">El Salvador</option>
 													 <option value="GQ">Equatorial Guinea</option>
 													 <option value="ER">Eritrea</option>
 													 <option value="EE">Estonia</option>
 													 <option value="ET">Ethiopia</option>
 													 <option value="FK">Falkland Islands (Malvinas)</option>
 													 <option value="FO">Faroe Islands</option>
 													 <option value="FJ">Fiji</option>
 													 <option value="FI">Finland</option>
 													 <option value="FR">France</option>
 													 <option value="GF">French Guiana</option>
 													 <option value="PF">French Polynesia</option>
 													 <option value="TF">French Southern Territories</option>
 													 <option value="GA">Gabon</option>
 													 <option value="GM">Gambia</option>
 													 <option value="GE">Georgia</option>
 													 <option value="DE">Germany</option>
 													 <option value="GH">Ghana</option>
 													 <option value="GI">Gibraltar</option>
 													 <option value="GR">Greece</option>
 													 <option value="GL">Greenland</option>
 													 <option value="GD">Grenada</option>
 													 <option value="GP">Guadeloupe</option>
 													 <option value="GU">Guam</option>
 													 <option value="GT">Guatemala</option>
 													 <option value="GG">Guernsey</option>
 													 <option value="GN">Guinea</option>
 													 <option value="GW">Guinea-Bissau</option>
 													 <option value="GY">Guyana</option>
 													 <option value="HT">Haiti</option>
 													 <option value="HM">Heard Island and McDonald Islands</option>
 													 <option value="VA">Holy See (Vatican City State)</option>
 													 <option value="HN">Honduras</option>
 													 <option value="HK">Hong Kong</option>
 													 <option value="HU">Hungary</option>
 													 <option value="IS">Iceland</option>
 													 <option value="IN">India</option>
 													 <option value="ID">Indonesia</option>
 													 <option value="IR">Iran, Islamic Republic of</option>
 													 <option value="IQ">Iraq</option>
 													 <option value="IE">Ireland</option>
 													 <option value="IM">Isle of Man</option>
 													 <option value="IL">Israel</option>
 													 <option value="IT">Italy</option>
 													 <option value="JM">Jamaica</option>
 													 <option value="JP">Japan</option>
 													 <option value="JE">Jersey</option>
 													 <option value="JO">Jordan</option>
 													 <option value="KZ">Kazakhstan</option>
 													 <option value="KE">Kenya</option>
 													 <option value="KI">Kiribati</option>
 													 <option value="KP">Korea, Democratic People's Republic of</option>
 													 <option value="KR">Korea, Republic of</option>
 													 <option value="KW">Kuwait</option>
 													 <option value="KG">Kyrgyzstan</option>
 													 <option value="LA">Lao People's Democratic Republic</option>
 													 <option value="LV">Latvia</option>
 													 <option value="LB">Lebanon</option>
 													 <option value="LS">Lesotho</option>
 													 <option value="LR">Liberia</option>
 													 <option value="LY">Libya</option>
 													 <option value="LI">Liechtenstein</option>
 													 <option value="LT">Lithuania</option>
 													 <option value="LU">Luxembourg</option>
 													 <option value="MO">Macao</option>
 													 <option value="MK">Macedonia, the former Yugoslav Republic of</option>
 													 <option value="MG">Madagascar</option>
 													 <option value="MW">Malawi</option>
 													 <option value="MY">Malaysia</option>
 													 <option value="MV">Maldives</option>
 													 <option value="ML">Mali</option>
 													 <option value="MT">Malta</option>
 													 <option value="MH">Marshall Islands</option>
 													 <option value="MQ">Martinique</option>
 													 <option value="MR">Mauritania</option>
 													 <option value="MU">Mauritius</option>
 													 <option value="YT">Mayotte</option>
 													 <option value="MX">Mexico</option>
 													 <option value="FM">Micronesia, Federated States of</option>
 													 <option value="MD">Moldova, Republic of</option>
 													 <option value="MC">Monaco</option>
 													 <option value="MN">Mongolia</option>
 													 <option value="ME">Montenegro</option>
 													 <option value="MS">Montserrat</option>
 													 <option value="MA">Morocco</option>
 													 <option value="MZ">Mozambique</option>
 													 <option value="MM">Myanmar</option>
 													 <option value="NA">Namibia</option>
 													 <option value="NR">Nauru</option>
 													 <option value="NP">Nepal</option>
 													 <option value="NL">Netherlands</option>
 													 <option value="NC">New Caledonia</option>
 													 <option value="NZ">New Zealand</option>
 													 <option value="NI">Nicaragua</option>
 													 <option value="NE">Niger</option>
 													 <option value="NG">Nigeria</option>
 													 <option value="NU">Niue</option>
 													 <option value="NF">Norfolk Island</option>
 													 <option value="MP">Northern Mariana Islands</option>
 													 <option value="NO">Norway</option>
 													 <option value="OM">Oman</option>
 													 <option value="PK">Pakistan</option>
 													 <option value="PW">Palau</option>
 													 <option value="PS">Palestinian Territory, Occupied</option>
 													 <option value="PA">Panama</option>
 													 <option value="PG">Papua New Guinea</option>
 													 <option value="PY">Paraguay</option>
 													 <option value="PE">Peru</option>
 													 <option value="PH">Philippines</option>
 													 <option value="PN">Pitcairn</option>
 													 <option value="PL">Poland</option>
 													 <option value="PT">Portugal</option>
 													 <option value="PR">Puerto Rico</option>
 													 <option value="QA">Qatar</option>
 													 <option value="RE">Réunion</option>
 													 <option value="RO">Romania</option>
 													 <option value="RU">Russian Federation</option>
 													 <option value="RW">Rwanda</option>
 													 <option value="BL">Saint Barthélemy</option>
 													 <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
 													 <option value="KN">Saint Kitts and Nevis</option>
 													 <option value="LC">Saint Lucia</option>
 													 <option value="MF">Saint Martin (French part)</option>
 													 <option value="PM">Saint Pierre and Miquelon</option>
 													 <option value="VC">Saint Vincent and the Grenadines</option>
 													 <option value="WS">Samoa</option>
 													 <option value="SM">San Marino</option>
 													 <option value="ST">Sao Tome and Principe</option>
 													 <option value="SA">Saudi Arabia</option>
 													 <option value="SN">Senegal</option>
 													 <option value="RS">Serbia</option>
 													 <option value="SC">Seychelles</option>
 													 <option value="SL">Sierra Leone</option>
 													 <option value="SG">Singapore</option>
 													 <option value="SX">Sint Maarten (Dutch part)</option>
 													 <option value="SK">Slovakia</option>
 													 <option value="SI">Slovenia</option>
 													 <option value="SB">Solomon Islands</option>
 													 <option value="SO">Somalia</option>
 													 <option value="ZA">South Africa</option>
 													 <option value="GS">South Georgia and the South Sandwich Islands</option>
 													 <option value="SS">South Sudan</option>
 													 <option value="ES">Spain</option>
 													 <option value="LK">Sri Lanka</option>
 													 <option value="SD">Sudan</option>
 													 <option value="SR">Suriname</option>
 													 <option value="SJ">Svalbard and Jan Mayen</option>
 													 <option value="SZ">Swaziland</option>
 													 <option value="SE">Sweden</option>
 													 <option value="CH">Switzerland</option>
 													 <option value="SY">Syrian Arab Republic</option>
 													 <option value="TW">Taiwan, Province of China</option>
 													 <option value="TJ">Tajikistan</option>
 													 <option value="TZ">Tanzania, United Republic of</option>
 													 <option value="TH">Thailand</option>
 													 <option value="TL">Timor-Leste</option>
 													 <option value="TG">Togo</option>
 													 <option value="TK">Tokelau</option>
 													 <option value="TO">Tonga</option>
 													 <option value="TT">Trinidad and Tobago</option>
 													 <option value="TN">Tunisia</option>
 													 <option value="TR">Turkey</option>
 													 <option value="TM">Turkmenistan</option>
 													 <option value="TC">Turks and Caicos Islands</option>
 													 <option value="TV">Tuvalu</option>
 													 <option value="UG">Uganda</option>
 													 <option value="UA">Ukraine</option>
 													 <option value="AE">United Arab Emirates</option>
 													 <option value="GB">United Kingdom</option>
 													 <option value="US">United States</option>
 													 <option value="UM">United States Minor Outlying Islands</option>
 													 <option value="UY">Uruguay</option>
 													 <option value="UZ">Uzbekistan</option>
 													 <option value="VU">Vanuatu</option>
 													 <option value="VE">Venezuela, Bolivarian Republic of</option>
 													 <option value="VN">Viet Nam</option>
 													 <option value="VG">Virgin Islands, British</option>
 													 <option value="VI">Virgin Islands, U.S.</option>
 													 <option value="WF">Wallis and Futuna</option>
 													 <option value="EH">Western Sahara</option>
 													 <option value="YE">Yemen</option>
 													 <option value="ZM">Zambia</option>
 													 <option value="ZW">Zimbabwe</option>
 												 </select>
 											 </div>
 										 </div>

 										 <div class="form-group">
 											 <label for="stateregion_edit" class="col-sm-2 control-label" for="stateregion">State / Region</label>
 											 <div class="col-sm-10">
 											 <input type="text" class="form-control underline" id="stateregion_edit" placeholder="State / Region" autofocus>
 											 <p class="errorstateregion text-center alert alert-danger hidden">The State or Region field is required.</p>
 											 </div>
 										 </div>

 										 <div class="form-group">
 											 <label for="phonenumber_edit" class="col-sm-2 control-label" for="phonenumber">Phone Number</label>
 											 <div class="col-sm-10">
 											 <input type="text" class="form-control underline" id="phonenumber_edit" placeholder="Phone Number" autofocus>
 											 <p class="errorphonenumber text-center alert alert-danger hidden">The Phone Number field is required.</p>
 											 </div>
 										 </div>

                  </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                             <span><i class="fa fa-check"></i></span> Edit
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <h3 class="text-center">Are you sure you want to delete this user?</h3>
                     <br />
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_delete" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="name">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_delete" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                             <span id=""><i class="fa fa-thrash"></i></span> Delete
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>
@include('compagnyAjax')

@stop
