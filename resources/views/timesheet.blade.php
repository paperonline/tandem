@extends('layouts.dashboard')

@section('page_title')
	Timesheet Management
@stop
@section('page_heading')
	Timesheet Management
@stop
@section('dashboard-content')


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    {{-- <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}"> --}}

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>


    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style>
        .panel-heading {
            padding: 0;
        }
        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .panel-heading li {
            float: left;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }
				table td {
				  position: relative;
				}

				table td input {
				  position: absolute;
				  display: block;
				  top:0;
				  left:0;
				  margin: 0;
				  height: 100%;
				  width: 100%;
				  border: none;
				  padding: 10px;
				  box-sizing: border-box;
				}
				.champs{
					width: 100%;
				}
        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }
    </style>

</head>

<body>
    <div class="col-sm-12">
        <!-- <h2 class="text-center">Timesheet</h2> -->
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
							<ul class="nav navbar-nav navbar-right top-nav" style="margin: 15px;">
								<div class="input-group">
										<input type="text" class="form-control" id="search" name="search" placeholder="search">
										<span class="input-group-btn"><button class="btn btn-default" type="button" id="searchBtn"><i class="fa fa-search"></i></button></span>
								</div>
							</ul>
							<ul>
									<li><button type="button" class="btn btn-success btn-bordered add-modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;Timesheet</button></li>
							</ul>
							<!-- <input type="text" class="form-controller" id="search" name="search" placeholder="search" style="color: black;"></input><i class="fa fa-search"></i> -->
            </div>
            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                        <thead>
                            <tr>
                                <th valign="middle">No. Carte</th>
                                <th>Nom de l'employé</th>
                                <th>Jours</th>
                                <th>Date</th>
                                <th>Absence</th>
																<th>Active</th>
                                <th>Admin</th>
                                <th style="width:93.95px;">Date Export</th>
                                <th>Temps Excuté</th>
                                <th>Actions</th>
                            </tr>
                          {{ csrf_field() }}
                        </thead>
                        <tbody id="tbodyListSheet">

													@foreach($listTimeSheet as $list)
                                <tr class="item{{$list->id}}">
                                    <td>{{$list->id}}</td>
                                    <td>{{$list->nomEmploye}}</td>
                                    <td>{{$list->jours}}</td>
                                    <td id="dateAff{{$list->id}}">{{$list->date}}</td>
                                    <td> - </td>
                                    <td class="text-center">
																			<input type="checkbox" style="position: static;" name="active" id="active{{$list->id}}" data-id="{{$list->id}}" {{  ($list->active == 1 ? ' checked' : '') }}  class="published"> </td>
                                    <td class="text-center">
																			@if(Auth::user()->role == 'administrateur')
																			<input type="checkbox" style="position: static;" name="adminExport" id="adminExport{{$list->id}}" data-id="{{$list->id}}" {{  ($list->exporte == 1 ? ' checked' : '') }} class="published">
																			@endif
																		</td>
                                    <td id="date_exporte{{$list->id}}"style="width:93.95px;">{{$list->date_exporte}}</td>
                                    <td>{{round($list->temps_excute,2)}} HRES</td>
                                    <td>
																			@php
																				$uuid = $list->uuidTimeSheet;
																				$uuidCount =  \DB::table('timesheets')->where('uuidTimeSheet','=',$uuid)->count();
																			@endphp
																			<input type="hidden" id="token" data-token="{{ csrf_token() }}">

																			@if ($list->exporte == 0)
																				<button class="edit-modal btn btn-info" id="editSheet{{$list->id}}" style="margin-bottom:10px;"
																				data-count = "{{$uuidCount}}"
																				data-uuid = "{{$list->uuidTimeSheet}}"
																				data-employe = "{{$list->nomEmploye}}"
																				data-date = "{{$list->date}}"
																				data-token = "{{ csrf_token() }}">
																				<span><i class="fa fa-edit"></i></span> Edit</button>
																			@else
																				<button class="edit-modal btn btn-info" id="editSheet" style="margin-bottom:10px;" disabled>
																				<span><i class="fa fa-edit"></i></span> Edit</button>
																			@endif


																				<button class="delete-modal btn btn-danger"
																				data-id="{{$list->id}}"
																				data-uuid = "{{$list->uuidTimeSheet}}"
																				data-date="{{$list->date}}"
																				data-name="{{$list->nomEmploye}}"
                                        <span><i class="fa fa-trash-o"></i></span> Delete</button>


                                    </td>
                                </tr>
																<script>
																$(document).ready(function(){

																	//search
																	$('#searchBtn').on('click',function(){
																		$value=$('#search').val();
																			$.ajax({
																				type : 'get',
																				url : "{{route('search')}}",
																				data:{'search':$value},
																				success:function(response){
																					var trHTML="";
																					console.log(response);

																					for(var i=0;i<response.output.length;i++){
																						var id = response.output[i].id;
																						var employe = response.output[i].nomEmploye;
																						var jours = response.output[i].jours;
																						var date = response.output[i].date;
																						var absence = response.output[i].absence;
																						var active = response.output[i].active;
																						var exporte = response.output[i].exporte;
																						var date_exporte = response.output[i].date_exporte;
																						var temps_excute = response.output[i].temps_excute;
																				let trRow='<tr>'+
																							'<td>'+id+'</td>'+
																							'<td>'+employe+'</td>'+
																							'<td>'+jours+'</td>'+
																							'<td>'+date+'</td>'+
																							'<td>'+absence+'</td>'+
																							'<td><input type="checkbox" style="position: static;" name="active" id="activeSearch{{$list->id}}" data-id="'+id+'" '+(active == 1 ? ' checked' :'""')+' class="published"></td></td>'+
																							'<td>@if(Auth::user()->role == 'administrateur')<input type="checkbox" style="position: static;" name="adminExport" id="adminExportSearch{{$list->id}}" data-id="'+id+'" '+(exporte == 1 ? ' checked' :'""')+'  class="published">@endif</td>'+
																							'<td id="date_exporteSearch{{$list->id}}" style="width:93.95px;">'+date_exporte+'</td>'+
																							'<td>'+temps_excute+'</td>'+
																							'<td>@php $uuid = $list->uuidTimeSheet; $uuidCount =  \DB::table('timesheets')->where('uuidTimeSheet','=',$uuid)->count();@endphp<input type="hidden" id="token" data-token="{{ csrf_token() }}">@if ($list->exporte == 0)<button class="edit-modal btn btn-info" id="editSheet{{$list->id}}"data-count = "{{$uuidCount}}"data-uuid = "{{$list->uuidTimeSheet}}"data-employe = "{{$list->nomEmploye}}"data-token = "{{ csrf_token() }}"><span><i class="fa fa-edit"></i></span> Edit</button>@else<button class="edit-modal btn btn-info" id="editSheet" disabled><span><i class="fa fa-edit"></i></span> Edit</button>@endif<button class="delete-modal btn btn-danger"data-id=""data-name=""data-email=""data-compagny=""data-role=""data-active=""><span><i class="fa fa-trash-o"></i></span> Delete</button></td>';

																							trHTML=trHTML+trRow
																					}
																					$('#tbodyListSheet').empty();
																					$('#tbodyListSheet').append(trHTML);

																				}
																			});

																	});


																 $('#tbodyListSheet').on('click', '#activeSearch{{$list->id}}', function(e) {
																	e.preventDefault();
																	var id = $('#activeSearch{{$list->id}}').data('id');
																	var token = $('#token').data('token');
																	if($('#activeSearch{{$list->id}}').is(":checked")) {
      																var check = 1;
																			var checked = true;
														        } else {
														           var check = 0;
																			 var checked = false;
														        }

																	$.ajax({
													            type: 'POST',
													            url: "{{route('active')}}",
													            data: {
													                '_token': token,
													                'id': id,
																					'check':check
													            },
													            dataType: 'json',

													            success: function(data) {
																					$('#activeSearch{{$list->id}}').prop("checked", checked);
																				}
													      	});
																});

																	var d = new Date();

																	var month = d.getMonth()+1;
																	var day = d.getDate();

																	var todayDate = ((''+day).length<2 ? '0' : '') + day + '-' +
																	    ((''+month).length<2 ? '0' : '') + month + '-' + d.getFullYear();

																			$('#active{{$list->id}}').on('click',function(e){
																				var id = $(this).data('id');
																				var token = $('#token').data('token');
																				if($(this).is(":checked")) {
	          																var check = 1;
																						var checked = true;
																	        } else {
																	           var check = 0;
																						 var checked = false;
																	        }
																        e.preventDefault();
																				$.ajax({
																            type: 'POST',
																            url: "{{route('active')}}",
																            data: {
																                '_token': token,
																                'id': id,
																								'check':check
																            },
																            dataType: 'json',

																            success: function(data) {
																								$('#active{{$list->id}}').prop("checked", checked);
																							}
																      	});
																			});

																			$('#tbodyListSheet').on('click','#adminExportSearch{{$list->id}}',function(e){
																				alert('allo');
																				var id = $('#adminExportSearch{{$list->id}}').data('id');
																				var token = $('#token').data('token');
																				if($('#adminExportSearch{{$list->id}}').is(":checked")) {
	          																var check = 1;
																						var checked = true;
																						var disabled = true;
																						var date = todayDate;
																	        } else {
																	           var check = 0;
																						 var checked = false;
																						 var disabled = false;
																						 var date = '-';
																	        }
																        e.preventDefault();
																				$.ajax({
																            type: 'POST',
																            url: "{{route('exportAdmin')}}",
																            data: {
																                '_token': token,
																                'id': id,
																								'check':check,
																								'todayDate': todayDate
																            },
																            dataType: 'json',

																            success: function(data) {
																								$('#adminExportSearch{{$list->id}}').prop("checked", checked);
																								$('#editSheet{{$list->id}}').prop("disabled", disabled);
																								$('#date_exporteSearch{{$list->id}}').html('<td >'+date+'</td>');
																							}
																      	});
																			});

																		$('#adminExport{{$list->id}}').on('click',function(e){
																			var id = $(this).data('id');
																			var token = $('#token').data('token');
																			if($(this).is(":checked")) {
          																var check = 1;
																					var checked = true;
																					var disabled = true;
																					var date = todayDate;
																        } else {
																           var check = 0;
																					 var checked = false;
																					 var disabled = false;
																					 var date = '-';
																        }
															        e.preventDefault();
																			$.ajax({
															            type: 'POST',
															            url: "{{route('exportAdmin')}}",
															            data: {
															                '_token': token,
															                'id': id,
																							'check':check,
																							'todayDate': todayDate
															            },
															            dataType: 'json',

															            success: function(data) {
																							$('#adminExport{{$list->id}}').prop("checked", checked);
																							$('#editSheet{{$list->id}}').prop("disabled", disabled);
																							$('#date_exporte{{$list->id}}').html('<td >'+date+'</td>');
																						}
															      	});
																		});
																});
																</script>
															@endforeach
                        </tbody>
                    </table>
										<ul class="nav nav-pills" style="display: flex;align-items: center;justify-content: right;">
											<li class="nav-item">
												<button id="export" class="btn btn-export" style="margin-right:20px; margin-bottom:5px;">Export as CSV</button>
											</li>
											<li class="nav-item">
												{{ $listTimeSheet->links() }}
											</li>
									</ul>

            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->






    <!-- Modal form to add a timesheet -->
     <div id="addModal" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg" style="width:100%;">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                   <div class="panel panel-default" style="border-color:#FFFFFF; margin-bottom:0px;">
                       <div class="panel-heading" style="background: #f8f5f500; border-color:#FFFFFF;">
											 </div>
                   <div class="table-responsive">
										 <form class="form-inline" method="post" id="dynamic_form">
											 {{ csrf_field() }}
											 <div class="input-group date" id="date" style="margin:20px;">
												 <input type="text" class="form-control" name="date_add" id="date_input" >
												 <div class="input-group-addon">
														 <span class="glyphicon glyphicon-th"></span>
												 </div>
											 </div>
                           <table class="table table-striped table-bordered table-hover" id="user_table" style="table-layout: fixed; word-wrap: break-word;">
                               <thead>
                                   <tr>
                                       <th class="tablecell" style="width: 50%" valign="middle">Dossier</th>
                                       <th class="tablecell" style="width: 50%">Client</th>
                                       <th class="tablecell" style="width: 50%">Titre tache</th>
                                       <th class="tablecell" style="width: 50%">Debut</th>
                                       <th class="tablecell" style="width: 50%">Fin</th>
                                       <th class="tablecell" style="width: 50%">Departement</th>
                                       <th class="tablecell" style="width: 50%">Tache</th>
                                       <th class="tablecell" style="width: 50%">Commentaire</th>
																			 <th class="tablecell" style="width: 50%">Action</th>
                                   </tr>

                               </thead>

                               <tbody id="addForm">

															 </tbody>

                            </table>
														<div class="modal-footer">
 														@csrf
			 											 <input type="submit" name="save" id="save" class="btn btn-primary" value="Save"/>
			                          <button type="button" class="btn btn-warning" data-dismiss="modal">
			                              <span><i class="fa fa-remove"></i></span> Close
			                          </button>
			                      </div>
														</form>
                         </div>
                      </div>
                 </div>
             </div>
         </div>
     </div>

<script>
var n;
$('#export').click(function() {
	window.location = '/timesheet-csv';
});

$(function () {
	$('#date').datetimepicker({
    defaultDate: new Date(),
    format:'DD/MM/YYYY',
	});
	var d = $("#date_input").val();
  var weekday = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  var n = weekday[d];

});

</script>


		     <!-- Modal form to edit a timesheet -->
		      <div id="editModal" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg" style="width:100%;">
		              <div class="modal-content">
		                  <div class="modal-header">
		                      <button type="button" class="close" data-dismiss="modal">×</button>
		                      <h4 class="modal-title"></h4>
		                  </div>
		                  <div class="modal-body">
		                    <div class="panel panel-default" style="border-color:#FFFFFF;">
                     	<div class="panel-heading" style="background: #f8f5f500; border-color:#FFFFFF;">

													</div>
		                    <div class="panel-body">
		 										 <form class="form-inline" id="modifSheet">
													 {{ method_field('PUT') }}

    									 			<!-- <input type="hidden" id="tokenEdit" name="_token" data-token="{{ csrf_token() }}"> -->
		 											 <span id="resultSheet"></span>
													 <table class="table table-striped table-bordered table-hover" id="user_table" style="table-layout: fixed; word-wrap: break-word;">
		                                <thead>
		 																 	<tr>
																					<div class="form-group" style="margin-bottom:10px;">
								                              <label class="control-label col-sm-2" for="date">Date:</label>
								                              <div class="col-sm-10">
								                                  <input type="name" class="form-control" id="date_edit" disabled>
								                              </div>
								                          </div>
		 																	</tr>
		                                    <tr>
																					<th class="tablecell" style="width: 50%" valign="middle">Dossier</th>
																					<th class="tablecell" style="width: 50%">Client</th>
																					<th class="tablecell" style="width: 50%">Titre tache</th>
																					<th class="tablecell" style="width: 50%">Debut</th>
																					<th class="tablecell" style="width: 50%">Fin</th>
																					<th class="tablecell" style="width: 50%">Departement</th>
																					<th class="tablecell" style="width: 50%">Tache</th>
																					<th class="tablecell" style="width: 50%">Commentaire</th>
																				 <th class="tablecell" style="width: 50%">Action</th>
		                                    </tr>

		                                </thead>

		                                <tbody id="tbody"></tbody>
																			@csrf
		                             </table>
																 </form>
		 														<div class="modal-footer">
																	<!-- <input type="button" name="save" id="editSave" class="btn btn-primary" value="Save"/> -->
																	<button type="button" class="btn btn-primary edit" id="editSave" >
				                              <span><i class="fa fa-check"></i></span> Edit
				                          </button>
				                          <button type="button" id="closeModif" class="btn btn-warning" data-dismiss="modal">
				                              <span><i class="fa fa-remove"></i></span> Close
				                          </button>
		 			                      </div>

		                          </div>
		                       </div>
		                  </div>
		              </div>
		          </div>
		      </div>
					<script>
					var n;

					$(function () {
						$('#date').datetimepicker({
					    defaultDate: new Date(),
					    format:'DD/MM/YYYY',
						});
						var d = $("#date_input").val();
					  var weekday = new Array(7);
					  weekday[0] = "Sunday";
					  weekday[1] = "Monday";
					  weekday[2] = "Tuesday";
					  weekday[3] = "Wednesday";
					  weekday[4] = "Thursday";
					  weekday[5] = "Friday";
					  weekday[6] = "Saturday";

					  var n = weekday[d];

					});

					</script>



     <!-- Modal form to show a post -->
     <div id="showModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="title">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_show" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="content">Email:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="email_show" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>


     <!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title"></h4>
                 </div>
                 <div class="modal-body">
                     <h3 class="text-center">Are you sure you want to delete this user?</h3>
                     <br />
                     <form class="form-horizontal" role="form">
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="id">ID:</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="id_delete" disabled>
                             </div>
                         </div>
												 <div class="form-group">
                             <label class="control-label col-sm-2" for="date">Date:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="date_delete" disabled>
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-sm-2" for="name">Name:</label>
                             <div class="col-sm-10">
                                 <input type="name" class="form-control" id="name_delete" disabled>
                             </div>
                         </div>
                     </form>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                             <span id=""><i class="fa fa-thrash"></i></span> Delete
                         </button>
                         <button type="button" class="btn btn-warning" data-dismiss="modal">
                             <span><i class="fa fa-remove"></i></span> Close
                         </button>
                     </div>
                 </div>
             </div>
         </div>
     </div>
@include('timesheetAjax')

@stop
