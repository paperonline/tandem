@extends('layouts.dashboard')



@section('page_title')
	Company Management
@stop
@section('page_subtitle')
Bootstrap UI Components
@stop
@section('page_heading')
	Company Management
@stop
@section('dashboard-content')

<div class="row">
	<div class="col-sm-12">
		 @section ('htable_panel_title','List of Companies')
	@section ('htable_panel_body')
		@include('widgets.tableComp', array('class'=>'table-hover'))
		@endsection
	 @include('widgets.panel', array('class'=>'success', 'controls'=>true, 'header'=>true, 'as'=>'htable'))
	</div>
</div>




		 <button type="button" class="btn btn-primary btn-rounded" style=" position: fixed; bottom: 20px; right: 20px; margin-bottom:40px;" data-toggle="modal" data-target="#myModal2">
			 <i class="fa fa-plus" style="font-size:20px;"></i>
		 </button>
		


 <!-- @include('widgets.panel', array('class'=>'primary', 'header'=>true, 'as'=>'modal1')) -->



@stop
