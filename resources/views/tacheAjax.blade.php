<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<!-- Bootstrap JavaScript -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

<!-- toastr notifications -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- icheck checkboxes -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js"></script>

<!-- Delay table load until everything else is loaded -->
<script>
    $(window).load(function(){
        $('#postTable').removeAttr('style');
    })
</script>


<!-- AJAX CRUD operations -->
<script type="text/javascript">
      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
    // add a new post
    $(document).on('click', '.add-modal', function() {
        $('.modal-title').text('Add');
        $('#addModal').modal('show');

    });


    $('.modal-footer').on('click', '.add', function() {


        $.ajax({
            type: 'POST',
            url: "{{ route('tache.store')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'name': $('#name_add').val(),
                'code': $('#code_add').val(),
                'departements': $('#departements_add').val(),

            },
            success: function(data) {

                $('.errorTitle').addClass('hidden');
                $('.errorContent').addClass('hidden');

                if ((data.errors)) {
                    setTimeout(function () {
                        $('#addModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                    }, 500);

                    if (data.errors.code) {
                        $('.errorCode').removeClass('hidden');
                        $('.errorCode').text(data.errors.code);
                    }
                    if (data.errors.name) {
                        $('.errorName').removeClass('hidden');
                        $('.errorName').text(data.errors.name);
                    }
                } else {

                    toastr.success('Successfully added Post!', 'Success Alert', {timeOut: 5000});
                    $('#postTable').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.code+ "</td><td>" + data.name + "</td><td>" + data.departement_name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-code='" + data.code + "' data-name='" + data.name + "' data-departement='" + data.departement_name + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-code='" + data.code + "' data-name='" + data.name +  "' data-departement='" + data.departement_name + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");

                }
            },
        });
    });

    // Show a post
    // $(document).on('click', '.show-modal', function() {
    //     $('.modal-title').text('Show');
    //     $('#id_show').val($(this).data('id'));
    //     $('#name_show').val($(this).data('name'));
    //     $('#email_show').val($(this).data('email'));
    //     $('#showModal').modal('show');
    // });
    //
    // Edit a post
    $(document).on('click', '.edit-modal', function() {
        $('.modal-title').text('Edit');
        $('#id_edit').val($(this).data('id'));
        $('#code_edit').val($(this).data('code'));
        $('#name_edit').val($(this).data('name'));
        var trs = $('#tbody').children('tr').length;

        $('#editModal').modal('show');
    });
    $('.modal-footer').on('click', '.edit', function() {

        $.ajax({
            type: 'PUT',
            url: 'tache/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#id_edit").val(),
                'code': $('#code_edit').val(),
                'name': $('#name_edit').val(),
            },
            success: function(data) {
                $('.errorCode').addClass('hidden');
                $('.errorName').addClass('hidden');

                if ((data.errors)) {
                    setTimeout(function () {
                        $('#editModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                    }, 500);

                    if (data.errors.code) {
                        $('.errorCode').removeClass('hidden');
                        $('.errorCode').text(data.errors.code);
                    }
                    if (data.errors.name) {
                        $('.errorName').removeClass('hidden');
                        $('.errorName').text(data.errors.name);
                    }
                } else {
                    toastr.success('Successfully updated Post!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.code+ "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-code='" + data.code + "' data-name='" + data.name + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-code='" + data.code + "' data-name='" + data.name + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");
                }
            }
        });
    });

    // delete a post
    $(document).on('click', '.delete-modal', function() {
        $('.modal-title').text('Delete');
        $('#id_delete').val($(this).data('id'));
        $('#name_delete').val($(this).data('name'));
        $('#deleteModal').modal('show');
        id = $('#id_delete').val();
    });
    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'DELETE',
            url: 'tache/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                $('.item' + data['id']).remove();
            }
        });
    });
</script>

</body>
</html>
