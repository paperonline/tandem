<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<!-- Bootstrap JavaScript -->
<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script> -->

<!-- toastr notifications -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- icheck checkboxes -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js"></script>

<!-- Delay table load until everything else is loaded -->
<script>
    $(window).load(function(){
        $('#postTable').removeAttr('style');
    })
</script>

<!-- AJAX CRUD operations -->
<script type="text/javascript">
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var count = 1;
var i = 1;
dynamic_field(count);

function dynamic_field(number)
{
        var html = '<tr>';
         html += '<td><input class="form-control input-sm champs" id="dossier'+count+'" style="width: 80%; margin: 10px;" type="text" name="dossier[]"/><div><p class="errorDossier text-center alert alert-danger hidden">Required.</p></div></td>';
         html += '<td><input class="form-control input-sm champs" id="client'+count+'"  style="width: 80%; margin: 10px;" type="text" name="client[]"/></td>';
         html += '<td><input class="form-control input-sm champs" id="titreTache'+count+'" style="width: 80%; margin: 10px;" type="text" name="titreTache[]"/></td>';
         html += '<td><input class="form-control input-sm champs" id="debutHr'+count+'" style="width: 80%; margin: 10px;" type="text" id="timepicker" name="debutHr[]" placeholder="HH:MM"/></td>';
         html += '<td><input class="form-control input-sm champs" id="finHr'+count+'"style="width: 80%; margin: 10px;" type="text" id="timepicker" name="finHr[]" placeholder="HH:MM"/></td>';
         html += '<td><select class="form-control input-sm champs" id="departement_add'+count+'" data-id="'+i+'" style="width: 100%;" name="departement[]">value=<option selected >Choisir...</option>@foreach($departement as $depart)<option>{{$depart->nom_departement}}</option>@endforeach</select></td>';
         html += '<td><select class="form-control input-sm champs" id="tache_add'+count+'" style="width: 100%;" name="tache[]">value=<option selected >Choisir...</option>@if(isset($selectTache))@foreach($selectTache as $tache)<option>{{$tache->nom_tache}} {{$tache->code_tache}}</option>@endforeach@else{''}@endif</select></td>';
         html += '<td><input class="form-control input-sm champs" style="width: 80%; margin: 10px;" type="text" name="comment[]"/></td>';

        if(number > 1)
         {
             html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
             $('#addForm').append(html);
         }
         else
         {
             html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
             $('#addForm').html(html);
         }
        i++;
         $("#departement_add"+count+"").change(function(e) {
             e.preventDefault();
             var dataId = $(this).data('id');
             var selected = $("#departement_add"+dataId+" option:selected").text();

              $.ajax({
                   type: 'POST',
                   url: "{{ route('selectTache')}}",
                   data: {
                       '_token': $('input[name=_token]').val(),
                       'selectDepart': selected,
                   },
                   success: function(response) {
                    var toAppend='<option value="">Choisir...</option>>'
                     for(var i=0;i<response.selectTache.length;i++){
                       var id = response.selectTache[i].id;
                       var tache = response.selectTache[i].nom_tache;
                       var code = response.selectTache[i].code_tache;
                       toAppend+='<option value="'+tache+'">'+ code +' '+ tache +'</option>>';
                     }
                     $("#tache_add"+dataId+"").html(toAppend);
                   }
               });
         });

 }



     $('#addForm').on('click', '#add', function(){
         count++;
         dynamic_field(count);

     });

    $('#addForm').on('click', '.remove', function(){
         count--;
         $(this).closest("tr").remove();
    });

   $('#editForm').on('click', '.remove', function(){
        count--;
        $(this).closest("tr").remove();
   });
 var y = 1;
   // Add modal --  nouvelle feuille temps
       $(document).on('click', '.add-modal', function() {
           <?php $dateExitant = date('d/m/Y');
           $lastSheet = $uuidCount =  \DB::table('list_time_sheets')->orderBy('created_at', 'id')->first();?>
           dateExistant = "<?=$dateExitant?>";
           lastSheet = "<?=$lastSheet->id?>";
           openSheet = $('#dateAff'+lastSheet+'').text();
           if (openSheet == dateExistant)
           {
             $('.modal-title').text('Add');
             $('#id_edit').val($(this).data('id'));


             var uuid = $('#editSheet'+lastSheet+'').data('uuid');
             var token = $('#editSheet'+lastSheet+'').data('token');

             $.ajax({
                 type: 'POST',
                 url: "{{route('getuuid')}}",
                 data: {
                     '_token': token,
                     'uuid': uuid,
                 },
                 dataType: 'json',

                 success: function(response) {
                     var trHTML="";
                     for(var i=0;i<response.output.length;i++){
                       var id = response.output[i].id;
                       var dossier = response.output[i].dossier;
                       var client= response.output[i].client;
                       var titreTache=response.output[i].titreTache;
                       var debut=response.output[i].debut;
                       var fin=response.output[i].fin;
                       var departement=response.output[i].departement;
                       var codeTache=response.output[i].codeTache;
                       var comment=response.output[i].comment;

                         let trRow='<tr>'+
                                   '<td><input type="text" name="dossier[]" align="middle" style="width: 80%; margin: 10px;" data-dossier="dossier" class="form-control" value="'+dossier+'" /></td>'+
                                   '<td><input type="text" name="client[]" style="width: 80%; margin: 10px;;" id="client" class="form-control" value="'+client+'" /></td>'+
                                   '<td><input type="text" name="titreTache[]" style="width: 80%; margin: 10px;;" id="titreTache" class="form-control" value="'+titreTache+'" /></td>'+
                                   '<td><input type="text" name="debut[]" style="width: 80%; margin: 10px;" id="debut" class="form-control" value="'+debut+'" placeholder="HH:MM" /></td>'+
                                   '<td><input type="text" name="fin[]" style="width: 80%; margin: 10px;" id="fin" class="form-control" value="'+fin+'" placeholder="HH:MM" /></td>'+
                                   '<td><select class="form-control" style="width: 100%;" name="departement[]"><value="'+departement+'">@foreach($departement as $depart)<option>{{$depart->nom_departement}}</option>@endforeach</select></td>'+
                                   '<td><select class="form-control" style="width: 100%;" name="codeTache["><value="'+codeTache+'">@foreach($taches as $tache)<option>{{$tache->nom_tache}}</option>@endforeach</select></td>'+
                                   '<td><input type="text" name="comment[]" style="width: 80%; margin: 10px;" id="comment" class="form-control" value="'+comment+'" /></td>'
                                   +"</tr>";

                                   if(y > 1)
                                    {
                                        trRow += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                                        $('#addForm').append(trRow);
                                    }
                                    else
                                    {
                                        trRow += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                                        $('#addForm').html(trRow);
                                    }
                        trHTML=trHTML+trRow;
                      }
                      y++;
                      $('#tbody').empty();
                      $('#tbody').append(trHTML);
                 }
             });

             $('#editModal').modal('show');
           }
           else
           {
             $('.modal-title').text('Add');
             $('#addModal').modal('show');
           }
       });



// Edit Modal -- modif feuille temps
    $(document).on('click', '.edit-modal', function(e) {
        e.preventDefault();
        $('.modal-title').text('Edit');
        $('#id_edit').val($(this).data('id'));
        $('#date_edit').val($(this).data('date'));
        var uuid = $(this).data('uuid');
        var employe = $(this).data('employe');
        var token = $(this).data('token');
        var count2 = 0;
        var x = 0;
        var a = 0;
        $.ajax({
            type: 'POST',
            url: "{{route('getuuid')}}",
            data: {
                '_token': token,
                'uuid': uuid,
                'employe': employe,
            },
            dataType: 'json',

            success: function(response) {
                var trHTML="";
                var toAppend;
                for(var i=0;i<response.output.length;i++){
                  var id = response.output[i].id;
                  var dossier = response.output[i].dossier;
                  var client= response.output[i].client;
                  var titreTache=response.output[i].titreTache;
                  var debut=response.output[i].debut;
                  var fin=response.output[i].fin;
                  var departement=response.output[i].departement;
                  var codeTache=response.output[i].codeTache;
                  var comment=response.output[i].comment;
                  var uuid=response.output[i].uuid;
                  var employe=response.output[i].employe;
                    let trRow='<tr id="trId'+id+'">'+
                              '<td><input type="text" name="dossier[]" align="middle" style="width: 80%; margin: 10px;" data-dossier="dossier" class="form-control" value="'+dossier+'" /></td>'+
                              '<td><input type="text" name="client[]" style="width: 80%; margin: 10px;;" id="client" class="form-control" value="'+client+'" /></td>'+
                              '<td><input type="text" name="titreTache[]" style="width: 80%; margin: 10px;;" id="titreTache" class="form-control" value="'+titreTache+'" /></td>'+
                              '<td><input type="text" name="debut[]" style="width: 80%; margin: 10px;" id="debut" class="form-control" value="'+debut+'" placeholder="HH:MM" /></td>'+
                              '<td><input type="text" name="fin[]" style="width: 80%; margin: 10px;" id="fin" class="form-control" value="'+fin+'" placeholder="HH:MM" /></td>'+
                              '<td><select class="form-control input-sm champs" id="departement'+x+'" data-id="'+x+'" data-depart="'+departement+'" style="width: 100%;" name="departement[]" >@foreach($departement as $depart)<option value="{{$depart->nom_departement}}">{{$depart->nom_departement}}</option>@endforeach</select></td>'+
                              '<td><select class="form-control input-sm champs" style="width: 100%;" id="tache_edit'+x+'" data-tache="'+codeTache+'" name="codeTache[]">@foreach($taches as $tache)<option value="{{$tache->code_tache}} {{$tache->nom_tache}}">{{$tache->code_tache}} {{$tache->nom_tache}}</option>@endforeach</select></td>'+
                              '<td><input type="text" name="comment[]" style="width: 80%; margin: 10px;" id="comment" class="form-control" value="'+comment+'" /></td>'+
                              '<input type="hidden" name="id[]" id="id_edit'+i+'" data-id="'+id+'" value="'+id+'" />'+
                              '<input type="hidden" name="uuid[]" id="uuid_edit" data-uuid="'+uuid+'" />'+
                              '<input type="hidden" name="employe[]" id="employe_edit" value="'+employe+'" />';
                    toAppend+='<option value="'+departement+'selected">{{$depart->nom_departement}}</option>>';
                   count2++;

                   if(count2 > 1)
                    {
                        tr = '<td><button type="button" name="remove" id="remove'+x+'" data-id="'+id+'" class="btn btn-danger remove">Remove</button></td>';
                        //$('#addForm').append(tr);

                    }
                    else
                    {
                        tr = '<td><button type="button" name="add" id="addEdit" class="btn btn-success">Add</button></td>';
                        //$('#addForm').append(tr2);
                    }
                    trHTML=trHTML+trRow+tr;

                    $('#tbody').on('click', '#remove'+x+'', function(){
                          var id = $(this).data('id');
                          if (confirm("Are you sure?")) {
                            $.ajax({
                                type: 'POST',
                                url: "{{ route('deleteSheet')}}",
                                data: {
                                    '_token': $('input[name=_token]').val(),
                                    'id': id
                                },
                                success: function(data) {
                                    toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 3000});
                                    //$('.item' + data['id']).remove();
                                    $('#trId'+id+'').closest("tr").remove();

                                }
                            });
                          }
                          return false;
                     });

                     function value(response){
                      for (var a = 0; a < response.output.length; a++) {
                         var depart = $('#departement'+a+'').data('depart');
                         var tache = $('#tache_edit'+a+'').data('tache');
                            $('#departement'+a+' option[value='+depart+']').prop('selected', true);
                            var dataId = $('#departement'+a+'').data('id');
                            var selected = $('#departement'+a+'').find('option:selected').text();
                            //$('#tache_edit'+a+' option[value='+tache+']').prop('selected', true);
                            $.ajax({
                                 type: 'POST',
                                 url: "{{ route('selectTache')}}",
                                 data: {
                                     '_token': $('input[name=_token]').val(),
                                     'selectDepart': selected,
                                 },
                                 success: function(response) {
                                  var toAppend;
                                   for(var i=0;i<response.selectTache.length;i++){
                                     var id = response.selectTache[i].id;
                                     var tache = response.selectTache[i].nom_tache;
                                     var code = response.selectTache[i].code_tache;
                                     toAppend+='<option value="'+tache+'">'+ code +' '+ tache +'</option>>';
                                   }
                                   $("#tache_edit"+dataId+"").html(toAppend);
                                 }
                             });
                      }
                    };

                     $('#modifSheet').on('change','#departement'+x+'', function(e) {
                        e.preventDefault();
                        var dataId = $(this).data('id');
                        var selected = $(this).find('option:selected').text();

                         $.ajax({
                              type: 'POST',
                              url: "{{ route('selectTache')}}",
                              data: {
                                  '_token': $('input[name=_token]').val(),
                                  'selectDepart': selected,
                              },
                              success: function(response) {
                               var toAppend;
                                for(var i=0;i<response.selectTache.length;i++){
                                  var id = response.selectTache[i].id;
                                  var tache = response.selectTache[i].nom_tache;
                                  var code = response.selectTache[i].code_tache;
                                  toAppend+='<option value="'+tache+'">'+ code +' '+ tache +'</option>>';
                                }
                                $("#tache_edit"+dataId+"").html(toAppend);
                              }
                          });
                      });
                  x++;
                 }

                 $('#tbody').empty();
                 $('#tbody').append(trHTML);
                value(response);

              }
            });

        $('#editModal').modal('show');
    });

    $(document).ready(function() {

      var count = 1;
      var x = 2;
      var it= 1;
      $('#modifSheet').on('click', '#addEdit', function(){

          var idTr = count;
          addTr ='<tr id="addTr'+count+'">'+
          '<td><input class="form-control input-sm champs" id="dossier'+count+'" style="width: 80%; margin: 10px;" type="text" name="dossier[]"/></td>'
          +'<td><input class="form-control input-sm champs" id="client'+count+'"  style="width: 80%; margin: 10px;" type="text" name="client[]"/></td>'
          +'<td><input class="form-control input-sm champs" id="titreTache'+count+'" style="width: 80%; margin: 10px;" type="text" name="titreTache[]"/></td>'
          +'<td><input class="form-control input-sm champs" id="debutHr'+count+'" style="width: 80%; margin: 10px;" type="text" id="timepicker" name="debut[]" placeholder="HH:MM"/></td>'
          +'<td><input class="form-control input-sm champs" id="finHr'+count+'"style="width: 80%; margin: 10px;" type="text" id="timepicker" name="fin[]" placeholder="HH:MM"/></td>'
          +'<td><select class="form-control input-sm champs" id="departement_add'+x+'" data-id="'+x+'" style="width: 100%;" name="departement[]">value=<option selected >Choisir...</option>@foreach($departement as $depart)<option>{{$depart->nom_departement}}</option>@endforeach</select></td>'
          +'<td><select class="form-control input-sm champs" id="tache_add'+x+'" style="width: 100%;" name="codeTache[]">value=<option selected >Choisir...</option>@if(isset($selectTache))@foreach($selectTache as $tache)<option>{{$tache->nom_tache}} {{$tache->code_tache}}</option>@endforeach@else{''}@endif</select></td>'
          +'<td><input class="form-control input-sm champs" style="width: 80%; margin: 10px;" type="text" name="comment[]"/></td>'+
          '<input type="hidden" name="id[]" id="id_add'+it+'" data-id="" value="'+0+'" />'+'/tr';
          $('#tbody').append(addTr);
          count++;

          if(count > 1){

               tr = '<td><button type="button" name="remove" id="remove_add'+idTr+'" class="btn btn-danger remove">Remove</button></td>';
               $("#addTr"+idTr+"").append(tr);
           }

           $('#modifSheet').on('change','#departement_add'+x+'', function(e) {
               e.preventDefault();
               var dataId = $(this).data('id');
               var selected = $(this).find('option:selected').text();
                $.ajax({
                     type: 'POST',
                     url: "{{ route('selectTache')}}",
                     data: {
                         '_token': $('input[name=_token]').val(),
                         'selectDepart': selected,
                     },
                     success: function(response) {
                      var toAppend='<option value="">Choisir...</option>>'

                       for(var i=0;i<response.selectTache.length;i++){
                         var id = response.selectTache[i].id;
                         var tache = response.selectTache[i].nom_tache;
                         var code = response.selectTache[i].code_tache;
                         toAppend+='<option value="'+tache+'">'+ code +' '+ tache +'</option>>';
                       }
                        $("#tache_add"+dataId+"").html(toAppend);
                     }

                 });
           });

           var uuid = $('#uuid_edit').data('uuid');
            var post = $(this).serialize();
                $.ajax({
                  type: 'POST',
                  url: "{{route('addSheet')}}",
                  data: {
                      '_token': $('input[name=_token]').val(),
                      'uuid': uuid,
                  },
                  dataType:'json',
                  success:function(response)
                  {     var idAdd = response.id

                        var toAppend='<input type="hidden" name="id[]" id="id_add'+idTr+'" data-id="'+idAdd+'" value="'+0+'" />';
                        $('#id_add'+idTr+'').replaceWith(toAppend);
                  }
              });

              $('#modifSheet').on('click', '#remove_add'+it+'', function(){
                    var id = $('#id_add'+idTr+'').data('id');

                        if (confirm("Are you sure?")) {
                          $.ajax({
                              type: 'POST',
                              url: "{{ route('deleteSheet')}}",
                              data: {
                                  '_token': $('input[name=_token]').val(),
                                  'id': id
                              },
                              success: function(data) {
                                  toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 3000});
                                  $("#addTr"+idTr+"").closest("tr").remove();
                              }
                          });
                        }
                        return false;

               });
          it++;
          x++;

       });


      $('#timepicker').timepicker({
          timeFormat: 'h:mm',
          interval: 15,
          minTime: '10',
          dynamic: false,
          dropdown: false,
          scrollbar: true
      });


 });


 $('.modal-footer').on('click', '.edit', function(event) {
   event.preventDefault();
   var errors = 0;
   var count = 1;

   $("#modifSheet :input[type=text]").map(function(){
        if( !$(this).val() ) {
             $(this).css('background', '#f2dede');

                 if ($('#departement_add'+count+'').val() == 'Choisir...') {
                    $('#departement_add'+count+'').css('background', '#f2dede');
                 }
                 if ($('#tache_add'+count+'').val() == 'Choisir...') {
                    $('#tache_add'+count+'').css('background', '#f2dede');
                 }
                 count++;
             errors++;
       } else if ($(this).val()) {
             $(this).css('background', '');
             var postSheet = $('#modifSheet').serialize();

                $.ajax({
                   type: 'PUT',
                   url: "{{route('sheetupdate')}}",
                   data: postSheet,
                   dataType:'json',
                   beforeSend:function(){
                     $('#editSave').attr('data-dismiss','modal');
                   },

                   success:function(postSheet)
                   {   if(postSheet.error)
                       {
                          var error_html = '';
                          for(var count = 0; count<postSheet.error.length; count++)
                          {
                            error.html += '<p>'+postSheet.error+'</p>';
                          }
                          $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                       }
                       else
                       {
                         $('#resultSheet').html('<div class="alert alert-success">'+postSheet.success+'</div>');
                       }

                       $('#editSave').attr('disabled', false);
                   }
               });
             }
     });
});

$('#dynamic_form').on('submit', function(event){
        event.preventDefault();
        var errors = 0;
        var count = 1;

      $("#dynamic_form :input[type=text]").map(function(){
           if( !$(this).val() ) {
                $(this).css('background', '#f2dede');

                    if ($('#departement_add'+count+'').val() == 'Choisir...') {
                       $('#departement_add'+count+'').css('background', '#f2dede');
                    }
                    if ($('#tache_add'+count+'').val() == 'Choisir...') {
                       $('#tache_add'+count+'').css('background', '#f2dede');
                    }
                    count++;
                errors++;
          } else if ($(this).val()) {
                $(this).css('background', '');
          }
        });
        if(errors > 0){
            $('#errorwarn').text("All fields are required");
            return false;
        }
        var post = $(this).serialize();

            $.ajax({
              type: 'POST',
              url: "{{route('timesheet.store')}}",
              data: post,
              dataType:'json',
              beforeSend:function(){
                $('#addModal').modal('toggle');
              },
              success:function(data)
              {
                    if(data.error)
                    { setTimeout(function () {
                        $('#addModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);
                       var error_html = '';
                       $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                    }
                    else
                    {
                      dynamic_field(1);
                      $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                    }
                    $('#save').attr('disabled', false);
              }
          });
    });




    // Show a post
    $(document).on('click', '.show-modal', function() {
        $('.modal-title').text('Show');
        $('#id_show').val($(this).data('id'));
        $('#name_show').val($(this).data('name'));
        $('#email_show').val($(this).data('email'));
        $('#showModal').modal('show');
    });

  // delete a post
    $(document).on('click', '.delete-modal', function() {
        $('.modal-title').text('Delete');
        $('#id_delete').val($(this).data('id'));
        uuid = $(this).data('uuid');
        $('#date_delete').val($(this).data('date'));
        $('#name_delete').val($(this).data('name'));
        $('#deleteModal').modal('show');
        id = $('#id_delete').val();
    });

    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'DELETE',
            url: 'timesheet/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id,
                'uuid':uuid
            },
            success: function(data) {
                toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 5000});
                $('.item' + data['id']).remove();
            }
        });
    });

</script>

</body>
</html>
