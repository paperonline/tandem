<div class="modal fade" id="modalCompUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">

      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel"></h4>
    </div>

    <div class="modal-body">
      <div class="conter-wrapper">
      <div class="row">
      <div class="col-md-12">
      <div class="panel panel-primary">
      <div class="panel-body">
      <form id="formUpdate" class="form-horizontal" method="POST" action="{{ route('register') }}" novalidate>
        @csrf
        <div class="form-group">
        <label for="inputtext" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
        <input type="text" class="form-control underline" name="name" id="name" placeholder="Compagny Name">
        </div>
        </div>

      <hr/>

        <div class="form-group">
        <label for="inputtext" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
        <input type="text" class="form-control underline" name="email" id="Email" placeholder="Email">
        </div>
        </div>

      <hr/>

        <div class="form-group">
        <label for="inputtext" class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
        <input type="text" class="form-control underline" name="Address" id="Address" placeholder="Address">
        </div>
        </div>

      <hr/>

      </div>
      </div>
      </div>
      </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary btn-rounded" value="Publier">Update</button>
    </div>
    </form>
  </div>
</div>
</div>
