<table class="table {{ $class }}">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Address</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($compagny as $comp)
		<tr>
			<td>{{ $comp->name }}</td>
			<td>{{ $comp->email }}</td>
			<td>{{ $comp->Address }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
