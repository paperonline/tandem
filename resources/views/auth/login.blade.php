@extends('layouts.app')

@section('content')
<div class="back">
<div class="errors col-sm-8 col-sm-offset-2">
</div>
<div class="login-outer">
<div class="login-wrap">
<div class="login-left striped-bg text-center">
<!-- <a href="url( '{{ url ('img/profile-cover.jpg') }} ')" class="logo-lg"> -->


<img class="logo-lg" src="{{ url ('img/tandem_nb.png') }}" alt="">
</a>
<!-- <div class="slogan">Theme</div> -->
</div>
<div class="login-right striped-bg">
<div class="heading">Login to your Account</div>
<div class="input">

<form class="form-horizontal" method="POST" role="form" action="{{ route('login') }}">
	@csrf
	<input type="hidden" name="" value="">

	<div class="login-input-group">
		<span class="login-input-group-addon"><i class="fa fa-at fa-fw"></i></span>
		<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" style="color:#ffffff;" name="email" value="{{ old('email') }}" required autofocus>
		@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('email') }}</strong>
				</span>
		@endif
	</div>

	<div class="login-input-group">
			<span class="login-input-group-addon"><i class="fa fa-key fa-fw"></i></span>
			<input id="password" type="password" style="color:#ffffff;" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
			@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
					</span>
			@endif
	</div>
	<input class="btn btn-default btn-lg submit" type="submit" value="Connexion">
	<!-- <a type="" class="btn btn-default btn-lg submit" href="../index.html">Login</a> -->
</form>

</div>
</div>
</div>
</div>
@endsection
