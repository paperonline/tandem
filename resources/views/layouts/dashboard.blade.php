@extends('layouts.app')

@section('body')
	<div id="app-container">
		<nav class="navbar navbar-inverse navbar-fixed-top striped-bg" id="top-navbar">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <!-- <a class="sidenav-toggle" href="#"><span class="brandbar"><i class="fa fa-bars hidd"></i></a></span> -->
		    </div>
			<div class="right-admin">
				<ul>
					<li class="dropdown hidd">
			          	<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			          		<img class="img-circle admin-img" src="{{ url ('img/user-icon.png') }}" alt="">
									</a>
			          	 <ul class="dropdown-menu admin" role="menu">
				          	<li role="presentation" class="dropdown-header">{{Auth::user()->name}}</li>
				            <!-- <li><a href="{{ route('profile') }}"><i class="fa fa-info"></i> Profile</a></li> -->
				            <li><a href="{{ route('login') }}"><i class="fa fa-power-off"></i> Logout</a></li>
				        </ul>
			        </li>
		        </ul>
			</div>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right top-nav">
		        <li class="dropdown">
		          	<a href="#" class="dropdown-toggle admin-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		          		<img class="img-circle admin-img" src="{{ url ('img/user-icon.png') }}" alt="">&nbsp;&nbsp;&nbsp;<span class="add">&nbsp;
						<i class="fa fa-angle-down"></i></span>
		          	</a>
		          	<ul class="dropdown-menu admin" role="menu">
			          	<li role="presentation" class="dropdown-header">{{Auth::user()->name}}</li>
			            <!-- <li><a href="{{ url ('profile') }}"><i class="fa fa-info"></i> Profile</a></li> -->
			            <li><a href="{{ url ('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
			        </ul>
		        </li>
		       </ul>
		    </div>
		</div>
		</nav>
		<div class="striped-bg" id="sidenav">
			<div role="tabpanel" id="navTabs">
				<div class="sidebar-controllers">

					<div class="">
						<div class="tab-content-scroller tab-content sidebar-section-wrap">
						    <div role="tabpanel" class="tab-pane active" id="menu">
						    	<div class="photo-container text-center">
								    <a href="{{ url ('profile') }}">
								    	<img src="{{ url ('img/user-icon.png') }}" alt="" class="img-circle dash-profile" />
								    </a>
									<div class="t-p">
										<a href="{{ url ('profile') }}">{{Auth::user()->name}}</a>
									</div>
								</div>
						    	<div class="section-heading">Menus</div>
								<ul class="nav sidebar-nav ">
							        <li {{ (Request::is('/') ? 'class=active' : '') }} ><a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a>
							        </li>
											@if(Auth::user()->role == 'administrateur' || Auth::user()->role == 'god')
											<li><a href="{{ route('userController.index') }}"><i class="fa fa-user"></i> Users</a>
											@endif
											@if(Auth::user()->role == 'god')
											<li><a href="{{ route('compagny.index') }}"><i class="fa fa-building"></i> Compagny</a>
											@endif
											<li><a href="{{ route('timesheet.index') }}"><i class="fa fa-list-alt"></i> Timesheets</a>
											@if(Auth::user()->role == 'administrateur' || Auth::user()->role == 'god')
											<li><a href="{{ route('departement.index') }}"><i class="fa fa-group"></i> Departments</a>
											<li><a href="{{ route('tache.index') }}"><i class="fa fa-tasks"></i> Tasks</a>
											@endif
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="comments">
								<div class="section-heading">Members</div>
								<ul class="online-members">
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile1.jpg') }}" alt="">Kumar Sanket <i class="fa fa-circle pull-right text-success"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Kumar Pratik <i class="fa fa-circle pull-right text-success"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Megha Kumari <i class="fa fa-circle pull-right text-success"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Sankhadeep Roy <i class="fa fa-circle pull-right text-success"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile.jpg') }}" alt="">Suraj Ahmad Choudhury <i class="fa fa-circle pull-right text-success"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/profile1.jpg') }}" alt="">Kumar Sanket <i class="fa fa-circle pull-right text-warning"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Megha Kumari <i class="fa fa-circle pull-right text-warning"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Kumar Pratik <i class="fa fa-circle pull-right text-warning"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Mrigank Mridul <i class="fa fa-circle pull-right text-muted"></i></a>
									</li>
									<li><a href=""><img class="img-circle chat-image" src="{{ url ('img/user-icon.png') }}" alt="">Amith M S <i class="fa fa-circle pull-right text-muted"></i></a>
									</li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="charts">
								<div class="section-heading">Charts</div>
								<div class="section-content text-center">
									<h4>Today's View</h4>
										<div class="chart-container">
											<div id="sidebar-piechart"></div>
										</div>
									<h4>Today's Signups</h4>
										<div class="chart-container2">
											<div id="sidebar-barchart"></div>
										</div>
									<hr class="lighter">
										<div class="transaction">
										<h4 class="eft">Today's Signups</h4>
											@include('widgets.progress', array('class'=> '', 'value'=>'67.34%', 'badge'=>true))
											@include('widgets.progress', array('class'=> 'success', 'value'=>'87.95%', 'badge'=>true))
											@include('widgets.progress', array('class'=> 'warning', 'value'=>'27.64%', 'badge'=>true))
											@include('widgets.progress', array('class'=> 'danger', 'value'=>'12', 'badge'=>true))
										</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="calendar">
								<div class="section-heading">Today</div>
									<ul class="today-ul">
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
											</a>
										</li>
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
											</a>
										</li>
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '9:00 AM'))</div><div class="happened">lorem ipsum happened</div>
											</a>
										</li>
									</ul>
								<div class="section-heading">7th April 2015</div>
									<ul class="today-ul dead">
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
											</a>
										</li>
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
											</a>
										</li>
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '9:00 AM'))</div><div class="happened">lorem ipsum happened</div>
											</a>
										</li>
									</ul>
								<div class="section-heading">9th April 2015</div>
									<ul class="today-ul dead">
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '7:00 AM'))</div><div class="happened">something happened</div>
											</a>
										</li>
										<li>
											<a href=""> <div class="panel-here">@include('widgets.labels', array('class'=>'primary', 'value'=> '8:00 AM'))</div><div class="happened">something more happenned</div>
											</a>
										</li>
									</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="notification">
								<div class="section-heading">Notifications</div>
								<div class="notification-info">
									<ul class="notif-ul">
					          			<li>
							            	<a href="" class="notification-wrap">
							            		<div class="notification-media">
							            			<span class="fa-stack fa-lg">
												  		<i class="fa fa-circle fa-stack-2x text-warning"></i>
												  		<i class="fa fa-user fa-stack-1x fa-inverse"></i>
													</span>
													<div><span class="label label-danger">Urgent</span></div>
												</div>
												<div class="notification-info">
													<div class="time-info"><small><i class="fa fa-comments"></i> 2 hours ago</small></div>
													<h5>Heading </h5>
													<p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
												</div>
							            	</a>
							            </li>
						           		<li><a href="" class="notification-wrap">
						            		<div class="pull-left notification-media">
						            			<span class="fa-stack fa-lg">
											  		<i class="fa fa-circle fa-stack-2x text-primary"></i>
											  		<i class="fa fa-user fa-stack-1x fa-inverse"></i>
												</span>
												<div><span class="label label-info">New</span></div>
											</div>
											<div class="notification-info">
												<div class="time-info"><small><i class="fa fa-comments"></i> 23rd Dec 2014</small></div>
												<h5>Heading </h5>
												<p>Hey Anna! Sorry for delayed response. I've just finished reading the mail you sent couple of...</p>
											</div>
						            		</a>
						            	</li>
			          				</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div id="body-container">
			@if (trim($__env->yieldContent('page_heading')))
    		<div class="page-title clearfix">
    			<div class="pull-left">
    				<h1>@yield('page_heading')</h1>
    				<small class="subtitle">@yield ('page_subtitle')</small>
    			</div>
    			<ol class="breadcrumb pull-right">
			 		<li class="active">@yield('page_heading')</li>
			  		<li><a href="{{ url ('') }}"><i class="fa fa-tachometer"></i></a></li>
			 	</ol>
			</div>
    		@endif
			<!-- <div class="statswrap">
				@yield ('stats')
			</div> -->
			<div class="conter-wrapper">
				@yield('dashboard-content')
			</div>

			<div id="footer-wrap" class="footer">
				Copyright © 2014 Dashy Theme
				<span class="pull-right">
					<a href="javascript:;"><i class="fa fa-facebook-square"></i></a>
					<a href="javascript:;">&nbsp;<i class="fa fa-twitter-square"></i></a>
				</span>
			</div>
		</div>
	</div>

@stop

@section('js')
@parent

<script type="text/javascript">


	$(function(){

		// Sidebar Charts

		// Pie Chart
		var chart3 = c3.generate({
		   bindto: '#sidebar-piechart',
		    data: {

		        // iris data from R
		        columns: [
		            ['1', 36],
		            ['2', 54],
		            ['3', 12],
		        ],
		        type : 'pie',
		        onclick: function (d, i) { console.log("onclick", d, i); },
		        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
		        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    pie: {
		      expand: true
		    },
		    size: {
		      width: 140,
		      height: 140
		    },
		    tooltip: {
		      show: false
		    }

		});



		// Bar Chart
		var chart6 = c3.generate({
		    bindto: '#sidebar-barchart',
		    data: {
		        columns: [
		            ['data1', 30, 200, 100, 400, 250, 310, 90, 125, 50]
		        ],
		        type: 'bar'
		    },
		    bar: {
		        width: {
		            ratio: 0.8
		        }
		    },
		    size: {
		      width: 200,
		      height: 120
		    },
		    tooltip: {
		      show: false
		    },
		    color: {
		        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
		    },
		    axis: {
		      y: {
		        show: false,
		        color: '#ffffff'
		      }
		}
		});


		// Sidebar Tabs
		$('#navTabs .sidebar-top-nav a').click(function (e) {
		 	e.preventDefault()
		 	$(this).tab('show');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 10);

		});



		$('.subnav-toggle').click(function() {
			$(this).parent('.sidenav-dropdown').toggleClass('show-subnav');
			$(this).find('.fa-angle-down').toggleClass('fa-flip-vertical');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

		});

	    $('.sidenav-toggle').click(function() {
	        $('#app-container').toggleClass('push-right');

		 	setTimeout(function(){
				$('.tab-content-scroller').perfectScrollbar('update');
		 	}, 500);

	    });


	    // Boxed Layout Toggle
		$('#boxed-layout').click(function() {

	        $('body').toggleClass('box-section');

	        var hasClass = $('body').hasClass('box-section');

	        $.get('/api/change-layout?layout='+ (hasClass ? 'boxed': 'fluid'));

		});



		$('.tab-content-scroller').perfectScrollbar();

		$('.theme-picker').click(function() {
			changeTheme($(this).attr('data-theme'));
		});


	});

	function changeTheme(theme) {

		$('<link>')
		  .appendTo('head')
		  .attr({type : 'text/css', rel : 'stylesheet'})
		  .attr('href', '/css/app-'+theme+'.css');

			$.get('api/change-theme?theme='+theme);
	}


</script>



@stop
