<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<!-- Bootstrap JavaScript -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

<!-- toastr notifications -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- icheck checkboxes -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

<!-- Delay table load until everything else is loaded -->
<script>
    $(window).load(function(){
        $('#postTable').removeAttr('style');
    })
</script>


<!-- AJAX CRUD operations -->
<script type="text/javascript">
      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
    // add a new post
    $(document).on('click', '.add-modal', function() {
        $('.modal-title').text('Add');
        $('#addModal').modal('show');

    });

    $(document).ready(function(){
            $('.published').on('click', function(event){
                  id = $(this).data('id');
                  if($(this).is(":checked")) {
                        var active = 1;
                    } else {
                        var active = 0;
                    }
                  $.ajax({
                      type: 'POST',
                      url: "compagny/changeStatus",
                      data: {
                          '_token': $('input[name=_token]').val(),
                          'id': id,
                          'active': active
                      },
                      success: function(data) {
                          // empty
                      },
                  });
              });
              $('.published').on('ifToggled', function(event) {
                  $(this).closest('tr').toggleClass('warning');
              });
    });

    $('.modal-footer').on('click', '.add', function() {

      if($('#active_add').is(":checked")) {
            var active = 1;
        } else {
            var active = 0;
        }

        $.ajax({
            type: 'POST',
            url: "{{ route('compagny.store')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'active': active,
                'namecompagny': $('#namecompagny_add').val(),
                'firstname': $('#firstname_add').val(),
                'lastname': $('#lastname_add').val(),
                'email':$('#email_add').val(),
                'streetaddress': $('#streetaddress_add').val(),
                'city': $('#city_add').val(),
                'codepostal': $('#codepostal_add').val(),
                'stateregion': $('#stateregion_add').val(),
                'country': $('#country_add').val(),
                'phonenumber': $('#phonenumber_add').val(),

            },
            success: function(data) {

                $('.errornamecompagny').addClass('hidden');
                $('.errorfirstname').addClass('hidden');
                $('.errorlastname').addClass('hidden');
                $('.erroremail').addClass('hidden');
                $('.errorstreetaddress').addClass('hidden');
                $('.errorcity').addClass('hidden');
                $('.errorcodepostal').addClass('hidden');
                $('.errorcountry').addClass('hidden');
                $('.errorphonenumber').addClass('hidden');

                if ((data.errors)) {
                    setTimeout(function () {
                        $('#addModal').modal('show');
                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                    }, 500);

                    if (data.errors.namecompagny) {
                        $('.errornamecompagny').removeClass('hidden');
                        $('.errornamecompagny').text(data.errors.namecompagny);
                    }
                    if (data.errors.firstname) {
                        $('.errorfirstname').removeClass('hidden');
                        $('.errorfirstname').text(data.errors.firstname);
                    }
                    if (data.errors.lastname) {
                        $('.errorlastname').removeClass('hidden');
                        $('.errorlastname').text(data.errors.lastname);
                    }
                    if (data.errors.email) {
                        $('.erroremail').removeClass('hidden');
                        $('.erroremail').text(data.errors.email);
                    }
                    if (data.errors.streetaddress) {
                        $('.errorstreetaddress').removeClass('hidden');
                        $('.errorstreetaddress').text(data.errors.streetaddress);
                    }
                    if (data.errors.city) {
                        $('.errorcity').removeClass('hidden');
                        $('.errorcity').text(data.errors.city);
                    }
                    if (data.errors.codepostal) {
                        $('.errorcodepostal').removeClass('hidden');
                        $('.errorcodepostal').text(data.errors.codepostal);
                    }
                    if (data.errors.country) {
                        $('.errorcountry').removeClass('hidden');
                        $('.errorcountry').text(data.errors.country);
                    }
                    if (data.errors.phonenumber) {
                        $('.errorphonenumber').removeClass('hidden');
                        $('.errorphonenumber').text(data.errors.phonenumber);
                    }
                } else {

                    toastr.success('Successfully added Post!', 'Success Alert', {timeOut: 5000});
                    $('#postTable').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.namecompagny + "</td><td>" + data.firstname + "</td><td>" + data.lastname + "</td><td>" + data.email + "</td><td class='text-center'><input type='checkbox' id='active_add' class='published' "+data.active+"></td><td>Right now</td><td><button class='show-modal btn btn-success' data-id='" + data.id + "' data-namecompagny='" + data.namecompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "'><span><i class='fa fa-eye'></i></span> Show</button> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-namecompagny='" + data.namecompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "'data-active='" + data.active + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-namecompagny='" + data.namecompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");

                }
            },
        });
    });
    // Show a post
    $(document).on('click', '.show-modal', function() {
        $('.modal-title').text('Show');
        $('#id_show').val($(this).data('id'));
        $('#active_show').val($(this).data('active'));
        $('#namecompagny_show').val($(this).data('namecompagny'));
        $('#firstname_show').val($(this).data('firstname'));
        $('#lastname_show').val($(this).data('lastname'));
        $('#email_show').val($(this).data('email'));
        $('#streetaddress_show').val($(this).data('streetaddress'));
        $('#city_show').val($(this).data('city'));
        $('#stateregion_show').val($(this).data('stateregion'));
        $('#country_show').val($(this).data('country'));
        $('#phonenumber_show').val($(this).data('phonenumber'));
        $('#showModal').modal('show');
    });


    // Edit a post
    $(document).on('click', '.edit-modal', function() {


        // if($('#compActive{{$comp->id}}').prop("checked", checked)) {
        //       var active = 1;
        //   } else {
        //       var active = 0;
        //   }

        $('.modal-title').text('Edit');
        $('#id_edit').val($(this).data('id'));
        $('#namecompagny_edit').val($(this).data('namecompagny'));
        $('#firstname_edit').val($(this).data('firstname'));
        $('#lastname_edit').val($(this).data('lastname'));
        $('#email_edit').val($(this).data('email'));
        $('#streetaddress_edit').val($(this).data('streetaddress'));
        $('#city_edit').val($(this).data('city'));
        $('#codePostal_edit').val($(this).data('codepostal'));
        $('#stateregion_edit').val($(this).data('stateregion'));
        $('#country_edit').val($(this).data('country'));
        $('#phonenumber_edit').val($(this).data('phonenumber'));
        id = $('#id_edit').val();
        if($('#compActive'+id+'').is(":checked")) {
            var checked = true;
          } else {
             var checked = false;
          }
        $('#switch_edit').prop("checked", checked);
        $('#editModal').modal('show');
    });

    $('.modal-footer').on('click', '.edit', function() {
        $.ajax({
            type: 'PUT',
            url: 'compagny/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#id_edit").val(),
                'active': active,
                'namecompagny': $('#namecompagny_edit').val(),
                'firstname': $('#firstname_edit').val(),
                'lastname': $('#lastname_edit').val(),
                'email': $('#email_edit').val(),
                'streetaddress': $('#streetaddress_edit').val(),
                'city': $('#city_edit').val(),
                'stateregion': $('#stateregion_edit').val(),
                'country': $('#country_edit').val(),
                'phonenumber': $('#phonenumber_edit').val(),
            },
            success: function(data) {
              $('.errornamecompagny').addClass('hidden');
              $('.errorfirstname').addClass('hidden');
              $('.errorlastname').addClass('hidden');
              $('.erroremail').addClass('hidden');
              $('.errorstreetaddress').addClass('hidden');
              $('.errorcity').addClass('hidden');
              $('.errorcodepostal').addClass('hidden');
              $('.errorcountry').addClass('hidden');
              $('.errorphonenumber').addClass('hidden');

              if ((data.errors)) {
                  setTimeout(function () {
                      $('#addModal').modal('show');
                      toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                  }, 500);

                  if (data.errors.namecompagny) {
                      $('.errornamecompagny').removeClass('hidden');
                      $('.errornamecompagny').text(data.errors.namecompagny);
                  }
                  if (data.errors.firstname) {
                      $('.errorfirstname').removeClass('hidden');
                      $('.errorfirstname').text(data.errors.firstname);
                  }
                  if (data.errors.lastname) {
                      $('.errorlastname').removeClass('hidden');
                      $('.errorlastname').text(data.errors.lastname);
                  }
                  if (data.errors.email) {
                      $('.erroremail').removeClass('hidden');
                      $('.erroremail').text(data.errors.email);
                  }
                  if (data.errors.streetaddress) {
                      $('.errorstreetaddress').removeClass('hidden');
                      $('.errorstreetaddress').text(data.errors.streetaddress);
                  }
                  if (data.errors.city) {
                      $('.errorcity').removeClass('hidden');
                      $('.errorcity').text(data.errors.city);
                  }
                  if (data.errors.codepostal) {
                      $('.errorcodepostal').removeClass('hidden');
                      $('.errorcodepostal').text(data.errors.codepostal);
                  }
                  if (data.errors.country) {
                      $('.errorcountry').removeClass('hidden');
                      $('.errorcountry').text(data.errors.country);
                  }
                  if (data.errors.phonenumber) {
                      $('.errorphonenumber').removeClass('hidden');
                      $('.errorphonenumber').text(data.errors.phonenumber);
                  }
              } else {

                  toastr.success('Successfully added Post!', 'Success Alert', {timeOut: 5000});
                  $('#postTable').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.namecompagny + "</td><td>" + data.firstname + "</td><td>" + data.lastname + "</td><td>" + data.email + "</td><td>" + data.active + "</td><td>Right now</td><td><button class='show-modal btn btn-success' data-id='" + data.id + "' data-namecompagny='" + data.namecompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "' ' data-active='" + data.active + "><span><i class='fa fa-eye'></i></span> Show</button> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-nameCompagny='" + data.nameCompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "'><span><i class='fa fa-edit'></i></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-namecompagny='" + data.namecompagny + "' data-firstname='" + data.firstname + "'data-lastname='" + data.lastname + "'data-email='" + data.email + "'><span><i class='fa fa-trash-o'></i></span> Delete</button></td></tr>");

              }
            }
        });
    });

    // delete a post
    $(document).on('click', '.delete-modal', function() {
        $('.modal-title').text('Delete');
        $('#id_delete').val($(this).data('id'));
        $('#namecompagny_delete').val($(this).data('namecompagny'));
        $('#deleteModal').modal('show');
        id = $('#id_delete').val();
    });
    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'DELETE',
            url: 'compagny/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                $('.item' + data['id']).remove();
            }
        });
    });
</script>

</body>
</html>
